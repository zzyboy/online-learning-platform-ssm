package com.isoft.pms.controller;

import com.isoft.pms.bean.User;
import com.isoft.pms.service.IUserService;
import com.isoft.pms.service.IUserService;
import com.isoft.pms.service.impl.UserServiceImpl;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author huxiongwei
 */
@RestController
@RequestMapping("/user")
@CrossOrigin(maxAge = 3600)
@Api("用户管理接口")
public class UserController {
    @Autowired
    IUserService service = new UserServiceImpl();

    @PutMapping(value = "add")
    @ApiOperation(value = "添加用户")
    public AjaxResult addUser(@RequestBody User user){
        return AjaxResult.success("添加成功",service.addUser(user));
    }

    @PostMapping("update")
    @ApiOperation(value = "更新用户信息")
    public AjaxResult updateUser(@RequestBody User user){
        return AjaxResult.success("更新成功",service.updateUser(user));
    }

    @DeleteMapping("del")
    @ApiOperation(value = "删除用户")
    public AjaxResult deleteUser(Long id){
        return AjaxResult.success("删除成功",service.delUser(id));
    }


    @GetMapping("getAllUser")
    @ApiOperation(value = "获得所有的用户信息")
    public AjaxResult getAllUser() {
        return AjaxResult.success("查询成功",service.getAllUser());
    }

    @PostMapping("getUser")
    @ApiOperation(value="根据条件模糊查询用户")
    public AjaxResult getUser(@RequestBody User user) {
        System.out.println(user);
        return AjaxResult.success("查询成功",service.getUser(user));
    }

    @PostMapping("upstatus")
    @ApiOperation(value = "用户逻辑删除")
    public AjaxResult fakeDeleteUser(Long id){return AjaxResult.success("删除成功",service.fakeDeleteUser(id));}

    @GetMapping("judgeId")
    @ApiOperation(value = "判断id是否在数据库中")
    public AjaxResult userIsExitById(Long id){return AjaxResult.success("查询成功",service.userIsExitById(id));}
    //get请求时不能传json数据 需要进行路径拼接
    @GetMapping("getUserById/{id}")
    @ApiOperation(value = "通过用户id获得用户")
    public AjaxResult getUserById(@PathVariable("id") Long id) {
        User user = new User();
        user.setId(id);
        return AjaxResult.success("获得成功",service.getUser(user));
    }

    @ApiOperation("更改用户状态，停用或者正常")
    @PostMapping("/upStatus")
    public AjaxResult updateUserStatus(@RequestBody User user) {
        return AjaxResult.success("更新成功",service.updateUserStatus(user));
    }
}
