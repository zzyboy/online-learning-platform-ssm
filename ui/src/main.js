import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import Router from '@/router/router'
import 'element-plus/dist/index.css'
import * as ElIcons from '@element-plus/icons-vue'
import store from '@/store'
// import '@/assets/css/layout.scss'
// import '@/assets/css/custom.scss'

//创建vue实例在App.vue上
const app = createApp(App);
for (let name in ElIcons) {
    app.component(name,ElIcons[name])
}



//挂载element-plus
app.use(ElementPlus);
app.use(store);
app.use(Router);
//挂载ID为app的组件上
app.mount('#app');


