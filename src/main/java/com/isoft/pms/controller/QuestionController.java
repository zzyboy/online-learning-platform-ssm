package com.isoft.pms.controller;

import com.isoft.pms.bean.Question;
import com.isoft.pms.service.IQuestionService;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author shangzhiwei
 */

@RestController
@RequestMapping("/question")
@CrossOrigin(maxAge = 3600)
@Api(description = "问题管理接口")
public class QuestionController {
    @Autowired
    private IQuestionService service;

    @RequestMapping(value = "question-add",method = RequestMethod.PUT)
    @ApiOperation(value = "添加问题")
    public AjaxResult addQuestion(@RequestBody Question question){ return AjaxResult.success("添加成功",service.addQuestion(question)); }

    @ApiOperation(value= "查询所有题目信息")
    @GetMapping("/question-selectAllQuestion")
    public AjaxResult selectAllQuestion() { return AjaxResult.success("查询成功",service.selectAllQuestion()); }

    @DeleteMapping(value = "question-delete")
    @ApiOperation(value = "删除问题")
    public Map<String,Object> deleteByQuestionId(int id) {return service.deleteByQuestionId(id);}

    @PostMapping("question-update")
    @ApiOperation(value = "修改问题")
    public Map<String,Object> updateQuestion(@RequestBody Question question) {return service.updateQuestion(question);}

    @ApiOperation(value = "根据问题编号逻辑删除问题")
    @ResponseBody
    @DeleteMapping("/question-fakeDeleteById")
    public AjaxResult fakeDeleteQuestion(Long id){ return AjaxResult.success("逻辑删除成功",service.fakeDeleteQuestion(id)); }

    @ApiOperation(value="根据条件模糊查询问题信息")
    @ResponseBody
    @GetMapping("/question-selectQuestion")
    public AjaxResult selectQuestion(@RequestBody Question question) {
        return AjaxResult.success("查询成功", service.selectQuestion(question));
    }
}
