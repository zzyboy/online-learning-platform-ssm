package com.isoft.pms.unit;

/** 状态码枚举类
 * @author zhouzhenyuan
 */

public enum DeptCode implements BaseCode{
    /**
     * 部门名字为空
     */
    DEPT_NAME_ISNULL(1001),
    /**
     * 部门id为空
     */
    DEPT_ID_ISNULL(1002),
    /**
     * 部门父id为空
     */
    DEPT_PARENT_ID_ISNULL(1003),
    /**
     * 领导id为空
     */
    DEPT_LEADER_ID_ISNULL(1004),
    /**
     * 部门状态码为空
     */
    DEPT_STATUS_ISNULL(1005),
    /**
     * 部门名称长度超过50字
     */
    DEPT_NAME_SIZE_OUT50(1006),

    /**
     * 父部门id在数据库中不存在
     * */
    DEPT_PARENT_ID_NOT_EXIST(1007),
    /**
     * 领导id不存在
     */
    DEPT_LEADER_ID_NOT_EXIST(1008),
    /**
     * 部门状态码不存在 只能选0 1 2
     */
    DEPT_STATUS_NOT_EXIST(1009),
    /**
     * 部门备注超过200字
     */
    DEPT_REMARKS_SIZE_OUT200(1010),
    /**
     * 部门编号已经存在
     */
    DEPT_ID_IS_EXIST(1011),
    /**
     * 部门编号不存在
     */
    DEPT_ID_IS_NOT_EXIST(1012),
    ;
    private final int i;
    DeptCode(int i) {
        this.i = i;
    }
    @Override
    public int value() {
        return this.i;
    }
}
