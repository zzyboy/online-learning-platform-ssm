import request from "@/unit/request";

export function addQuestion(question) {
    return request({
        url:'/question/question-add',
        method:'put',
        data:question
    });
}

export function selectAllQuestion() {
    return request({
        url:'/question/question-selectAllQuestion',
        method:'get'
    });
}

export function deleteByQuestionId(id){
    return request({
        url:'/question/question-delete',
        method:'delete',
        data:id
    });
}

export function updateQuestion(question){
    return request({
        url:'/question/question-update',
        method:'post',
        data:question
    });
}

export function fakeDeleteQuestion(id){
    return request({
        url:`/question/question-fakeDeleteById?id=${id}`,
        method:'delete',
    });
}

export function selectQuestion(question){
    return request({
        url:'/question/question-selectQuestion',
        method:'get',
        data:question
    })
}