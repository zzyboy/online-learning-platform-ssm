package com.isoft.pms.controller;

import com.isoft.pms.bean.Job;
import com.isoft.pms.service.IJobService;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author shangzhiwei
 */

@RestController
@RequestMapping("/job")
@CrossOrigin(maxAge = 3600)
@Api("任务管理接口")
public class JobController {

    @Autowired
    private IJobService service;

    @ApiOperation(value = "添加任务")
    @PutMapping( "/job-add")
    public AjaxResult addJob(@RequestBody Job job){
        System.out.println(job.getStartTime()+"？？？");
        System.out.println(job);
        return AjaxResult.success("添加成功",service.addJob(job));
    }

    @ApiOperation(value= "查询所有任务信息")
    @GetMapping("/job-selectAllJob")
    public AjaxResult selectAllJob() { return AjaxResult.success("查询成功",service.selectAllJob()); }

    @ApiOperation(value = "删除任务")
    @DeleteMapping("/job-delete")
    public AjaxResult deleteJob(Long id) {
        return AjaxResult.success("删除成功",service.deleteJob(id));
    }

    @ApiOperation(value = "修改任务")
    @PostMapping("/job-update")
    public AjaxResult updateJob(@RequestBody Job job) {
        return AjaxResult.success("修改成功",service.updateJob(job));
    }

    @ApiOperation(value = "根据任务编号逻辑删除任务")
    @ResponseBody
    @DeleteMapping("/job-fakeDeleteById")
    public AjaxResult fakeDeleteJob(Long id){ return AjaxResult.success("逻辑删除成功",service.fakeDeleteJob(id)); }

    @ApiOperation(value="根据条件模糊查询任务信息")
    @ResponseBody
    @PostMapping("/job-selectJob")
    public AjaxResult selectJob(@RequestBody Job job) {
        return AjaxResult.success("查询成功", service.selectJob(job));
    }

}
