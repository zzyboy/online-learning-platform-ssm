package com.isoft.pms.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author gaixiankang
 * 学生实体类
 */
@ApiModel(value = "学生信息")
@Data
public class Student {

    @ApiModelProperty(value = "学生编号")
    private Long id;
    @ApiModelProperty(value = "用户编号")
    private Long userId;
    @ApiModelProperty(value = "学生号")
    private Long studentId;
    @ApiModelProperty(value = "学生姓名")
    private String studentName;
    @ApiModelProperty(value = "学生性别")
    private Integer studentSex;
    @ApiModelProperty(value = "专业编号")
    private Long majorId;
    @ApiModelProperty(value = "状态")
    private Integer status;


}
