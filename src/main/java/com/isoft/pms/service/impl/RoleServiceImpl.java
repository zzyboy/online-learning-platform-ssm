package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Role;
import com.isoft.pms.dao.IPermissionDao;
import com.isoft.pms.dao.IRoleDao;
import com.isoft.pms.service.IRoleService;
import com.isoft.pms.unit.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhouzhenyuan
 */
@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private IRoleDao iRoleDao;
    @Autowired
    private IPermissionDao iPermissionDao;
    @Override
    public boolean addRole(Role role) {
        return iRoleDao.addRole(role);
    }

    @Override
    public List<Role> getAllRole() {
        return iRoleDao.getRoles(new Role());
    }

    @Override
    public List<Role> getRole(Role role) {
        return iRoleDao.getRoles(role);
    }

    @Override
    public boolean updateRole(Role role) {
        return iRoleDao.updateRole(role);
    }

    @Override
    public boolean fakeDeleteRole(Long id) {
        Role role = new Role();
        role.setId(id);
        role.setStatus(Status.DELETE.value());
        return iRoleDao.updateRole(role);
    }

    @Override
    public boolean deleteRole(Long id) {
        return iRoleDao.deleteRole(id);
    }

    @Override
    public boolean addPermissionWithRole(Long permissionId, Long roleId) {
        return iPermissionDao.addPermissionWithRole(permissionId,roleId);
    }

    @Override
    public List<Role> getRoles(Role role) {
        return iRoleDao.getRole(role);
    }

    @Override
    public Role getRoleById(Long id) {
        Role role = new Role();
        role.setId(id);
        List<Role> roles = iRoleDao.getRole(role);
        return roles.get(0);
    }
}
