import request from "@/unit/request";

export function addUser(user){
    return request({
        url:'user/add',
        method:'put',
        data:user
    });
}

export function updateUser(user){
    return request({
        url:'user/update',
        method:'post',
        data:user
    })
}

export function deleteUser(id){
    return request({
        url:'user/del',
        method:'delete',
        data:id
    })
}

export function getAllUser(){
    return request({
        url:'user/getAllUser',
        method:'get'
    })
}

export function getUserById(id) {
    return request({
        url:`user/getUserById/${id}`,
        method:'get'
    })
}

export function getUser(data){
    return request({
        url:'user/getUser',
        method:'post',
        data
    })
}

export function fakeDeleteUser(id){
    return request({
        url:'user/upstatus',
        method:'post',
        data:id
    })
}

export function userIsExitById(id){
    return request({
        url:'user/judgeId',
        method:'get',
        data:id
    })
}

export function userPhoto(data) {
    return request({
        url:'',
        method:'post',
        data
    })
}

export function updateStatus(data) {
    return request({
        url:'/user/upStatus',
        method:'post',
        data
    })
}

