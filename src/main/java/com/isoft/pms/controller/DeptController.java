package com.isoft.pms.controller;

import com.isoft.pms.bean.Dept;
import com.isoft.pms.service.IDeptService;
import com.isoft.pms.service.impl.DeptServiceImpl;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhouzhenyuan
 */
@RestController
@RequestMapping("/dept")
@CrossOrigin
@Api(description = "部门管理测试接口")
public class DeptController {

    @Autowired
    IDeptService iDeptService = new DeptServiceImpl();

    @ApiOperation(value = "部门增加")
    @PutMapping("/dept-add")
    public AjaxResult addDept(@RequestBody Dept dept) {
        return AjaxResult.success("添加成功",iDeptService.addDept(dept));
    }

    @ApiOperation(value = "全部部门信息")
    @GetMapping("/dept-getAll")
    public AjaxResult getAllDept() {
        return AjaxResult.success("获得成功",iDeptService.getAllDept());
    }

    @ApiOperation(value = "更新部门")
    @PostMapping("/dept-update")
    public AjaxResult updateDept(@RequestBody Dept dept) {
        return  AjaxResult.success("更新成功",iDeptService.updateDept(dept));
    }

    @ApiOperation(value = "部门物理删除")
    @DeleteMapping("/dept-delete")
    public AjaxResult deleteDept(Long id) {
        return AjaxResult.success("删除成功",iDeptService.deleteDeptById(id));
    }

    @ApiOperation(value = "部门逻辑删除")
    @DeleteMapping("/dept-fakeDelete")
    public AjaxResult fakeDeleteDept(Long id) {
        return AjaxResult.success("删除成功",iDeptService.fakeDeleteDeptById(id));
    }

    @ApiOperation(value="模糊搜索部门")
    @PostMapping("/dept-get")
    public AjaxResult getDept(@RequestBody Dept dept) {
        return AjaxResult.success("获得成功",iDeptService.getDept(dept));
    }
}
