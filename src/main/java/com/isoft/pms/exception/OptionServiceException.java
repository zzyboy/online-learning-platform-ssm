package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.OptionCode;

/**
 * 选项业务异常类
 * @author shangzhiwei
 */
public class OptionServiceException extends BaseServiceException {

    public OptionServiceException(OptionCode code){
        super(code);
        putIt(OptionCode.OPTION_CONTENT_ISNULL,"选项内容为空").
                putIt(OptionCode.OPTION_STATUS_ISNULL,"状态码为空").
                putIt(OptionCode.OPTION_STATUS_NOT_EXIST,"状态码不存在");
    }
}
