package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Job;
import com.isoft.pms.dao.IJobDao;
import com.isoft.pms.exception.JobServiceException;
import com.isoft.pms.service.IJobService;
import com.isoft.pms.unit.JobCode;
import com.isoft.pms.unit.StringUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author shangzhiwei
 */

@Service
public class JobServiceImpl implements IJobService {

    @Autowired
    private IJobDao dao;

    @Override
    public boolean addJob(Job job) {
        return dao.addJob(job);
    }

    @Override
    public List<Job> selectAllJob() { return dao.selectJobs(new Job()); }

    @Override
    public boolean deleteJob(Long id) {
        return dao.deleteJob(id);
    }

    @Override
    public boolean updateJob(Job job) {
        return dao.updateJob(job);
    }

    @Override
    public boolean fakeDeleteJob(Long id) {
        return dao.fakeDeleteJob(id);
    }

    @Override
    public List<Job> selectJob(Job job) {
        return dao.selectJobs(job);
    }

}
