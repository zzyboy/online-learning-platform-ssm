package com.isoft.pms.dao.sql;

import com.isoft.pms.bean.Question;
import org.apache.ibatis.jdbc.SQL;

/**
 * @author shangzhiwei
 */
public class QuestionSQL {

    public String createUpdateQuestionSQL(final Question question){
        return new SQL(){{
            UPDATE("question");
            if(question.getQuestionContent() != null){
                SET("question_content=#{questionContent}");
            }
            if(question.getQuestionType() != null){
                SET("question_type=#{questionType}");
            }
            if(question.getAnswer() != null){
                SET("answer=#{answer}");
            }
            if(question.getScore() != null){
                SET("score=#{score}");
            }
            if(question.getLevel() != null){
                SET("level=#{level}");
            }
            if(question.getStatus() != 0){
                SET("status=#{status}");
            }
            if(question.getIsObjective() != null){
                SET("isobjective=#{isObjective}");
            }
            if(question.getRemark() != null){
                SET("remark=#{remark}");
            }
            WHERE("id="+question.getId());
        }}.toString();
    }
}
