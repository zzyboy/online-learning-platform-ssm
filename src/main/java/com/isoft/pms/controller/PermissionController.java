package com.isoft.pms.controller;


import com.isoft.pms.bean.Permission;
import com.isoft.pms.service.IPermissionService;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/** 权限管理层接口
 * @author zhouzhenyuan
 */
@Api(description = "权限管理接口")
@RestController
@RequestMapping("/permission")
@CrossOrigin(maxAge = 3600)
public class PermissionController {

    @Autowired
    private IPermissionService permissionService;

    @ApiOperation(value="获得全部权限")
    @GetMapping("/permission-getAll")
    public AjaxResult getAll() {
        return AjaxResult.success("获得成功",permissionService.getAllPermission());
    }

    @ApiOperation(value="模糊查询权限")
    @GetMapping("/permission-get")
    public AjaxResult get(@RequestBody Permission permission) {
        return AjaxResult.success("获得成功",permissionService.getPermission(permission));
    }

    @ApiOperation(value = "修改权限")
    @PostMapping("/permission-update")
    public AjaxResult update(@RequestBody Permission permission) {
        return AjaxResult.success("更新成功",permissionService.updatePermission(permission));
    }

    @ApiOperation(value = "删除权限")
    @DeleteMapping("/permission-delete")
    public AjaxResult delete(Long id) {
        return AjaxResult.success("删除成功",permissionService.deletePermission(id));
    }

    @ApiOperation(value="添加权限")
    @PutMapping("/permission-add")
    public AjaxResult add(@RequestBody Permission permission) {
        return AjaxResult.success("添加成功",permissionService.addPermission(permission));
    }


}
