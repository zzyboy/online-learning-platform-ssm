package com.isoft.pms.unit;

/**
 * 状态码枚举类
 * @author shangzhiwei
 */

public  enum QuestionCode implements BaseCode{
    /**
     * 状态码
     */
    QUESTION_CONTENT_ISNULL(7001),
    QUESTION_TYPE_ISNULL(7002),
    QUESTION_ANSWER_ISNULL(7003),
    QUESTION_SCORE_ISNULL(7004),
    QUESTION_USER_ID_NOT_EXIST(7005),
    QUESTION_TYPE_NOT_EXIST(7006),
    QUESTION_SCORE_LESSZERO(7007),
    QUESTION_MAJOR_ID_NOT_EXIST(7008),
    QUESTION_STATUS_ISNULL(7009),
    QUESTION_STATUS_NOT_EXIST(7010),
    QUESTION_ISOBJECTIVE_NOT_EXIST(7011),
    QUESTION_REMARK_SIZE_OUT200(7012),
    QUESTION_CONTENT_EXIST(7013),
    ;
    private final int i;
    QuestionCode(int i) {this.i = i; }
    @Override
    public int value() {return this.i;}
}
