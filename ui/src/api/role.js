import request from "@/unit/request";

export function getAllRole() {
    return request({
        url: '/role/role-getAll',
        method: 'get'
    });
}

export function updateRoleById(data) {
    return request({
        url: '/role/role-updateById',
        method:'post',
        data
    })
}

export function addRole(data) {
    return request({
        url:'/role/role-add',
        method:'put',
        data
    })
}

export function addRoleWithPermission(data) {
    return request({
        url:'role/role-addPermission',
        method:'put',
        data
    })
}

export function getRoleById(id) {
    return request({
        url:`role/role-getRoles/${id}`,
        method:'get'
    })
}
