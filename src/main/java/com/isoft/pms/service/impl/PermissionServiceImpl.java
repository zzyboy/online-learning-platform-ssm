package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Permission;
import com.isoft.pms.dao.IPermissionDao;
import com.isoft.pms.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhouzhenyuan
 */
@Service
public class PermissionServiceImpl implements IPermissionService {

    @Autowired
    IPermissionDao iPermissionDao;

    @Override
    public List<Permission> getAllPermission() {
        return iPermissionDao.getPermission(new Permission());
    }

    @Override
    public boolean addPermission(Permission permission) {
        return iPermissionDao.addPermission(permission);
    }

    @Override
    public boolean deletePermission(Long id) {
        return iPermissionDao.deletePermission(id);
    }

    @Override
    public boolean updatePermission(Permission permission) {
        return iPermissionDao.updatePermission(permission);
    }

    @Override
    public List<Permission> getPermission(Permission permission) {
        return iPermissionDao.getPermission(permission);
    }
}
