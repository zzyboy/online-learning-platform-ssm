package com.isoft.pms.dao;

import com.isoft.pms.bean.Question;
import com.isoft.pms.dao.sql.QuestionSQL;
import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * @author shangzhiwei
 */
@Mapper
public interface IQuestionDao {

    /**
     * 添加问题
     * @param question
     * @return
     */
    @Insert("insert into question(id,user_id,question_content,question_type,answer,score,level,major_id,status,isobjective,remark) values(#{id},#{userId},#{questionContent},#{questionType},#{answer},#{score},#{level},#{majorId},#{status},#{isObjective},#{remark})")
    boolean addQuestion(Question question);

    /**
     * 添加问题时，查询是否有重复问题
     * @param questionContent
     * @return
     */
    @Select("select * from question where question_content = #{questionContent}")
    Question selectByQuestionContent(String questionContent);

    /**
     * 根据id删除问题
     * @param id
     * @return
     */
    @Delete("delete from question where id = #{id}")
    int deleteByQuestionId(@Param("id") int id);


    /**
     * 查询所有问题
     * @param question
     * @return
     */
    List<Question> selectQuestions(Question question);

    /**
     * 根据id修改问题
     * @param question
     * @return
     */
    @UpdateProvider(type = QuestionSQL.class, method = "createUpdateQuestionSQL")
    int updateQuestion(Question question);

    /**
     * 根据id逻辑删除问题
     * @param id
     * @return
     */
    @Update("update question set status=1 where id=#{id}")
    boolean fakeDeleteQuestion(Long id);

}
