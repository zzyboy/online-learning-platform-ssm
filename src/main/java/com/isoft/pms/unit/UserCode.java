package com.isoft.pms.unit;

/** 状态码枚举类
 * @author huxiongwei
 */

public enum UserCode implements BaseCode{
    /**
    * 状态码
    * */
    USER_NAME_ISNULL(3001),
    /**
     * 该用户名不存在
     */
    USER_NAME_ERROR(3002),
    USER_NAME_EXIST(3003),
    USER_PASSWORD_ISNULL(3004),
    /**
     * 3005 密码错误
     */
    USER_PASSWORD_ERROR(3005),
    USER_PHOTO_SIZE_OUT10M(3006),
    USER_CREATER_ISNULL(3007),
    USER_CREATER_ERROR(3008),
    USER_STATUS_NOT_EXIST(3009),
    USER_STATUS_ISNULL(3010),
    USER_ROLE_ID_ISNULL(3011),
    USER_ROLE_ID_ERROR(3012),
    USER_DEPT_ID_ISNULL(3013),
    USER_DEPT_ID_ERROR(3014),
    USER_SALT_ISNULL(3015),
    USER_CREATETIME_ISNULL(3016),
    USER_NOT_LOGIN(3017),
    /**
     * 用户id为空
     */
    USER_ID_ISNULL(3018),
    ;
    private final int i;
    UserCode(int i){this.i=i;}
    @Override
    public int value(){return this.i;}
}
