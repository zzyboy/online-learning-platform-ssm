package com.isoft.pms.unit;

/** 角色异常码枚举类
 * @author zhouzhenyuan
 */

public enum RoleCode implements BaseCode {
    /**
     * 角色标识符已存在
     */
    ROLE_KEY_IS_EXIT(2001),
    /**
     * 角色表示符不存在
     */
    ROLE_KEY_IS_NOT_EXIT(2002),
    /**
     * 角色编号为空
     */
    ROLE_ID_ISNULL(2003),
    /**
     * 角色状态码为空
     */
    ROLE_STATUS_ISNULL(2004),
    /**
     * 角色状态码异常
     */
    ROLE_STATUS_IS_NOT_EXIT(2005),
    /**
     * 角色编号不存在
     */
    ROLE_ID_IS_NOT_EXIT(2006),
    /**
     * 角色编号已存在
     */
    ROLE_ID_IS_EXIT(2007),
    /**
     * 角色描述长度长度超过50
     */
    ROLE_DESCRIPTION_SIZE_OUT50(2008),
    /**
     * 角色表示符超过长度20
     */
    ROLE_KEY_SIZE_OUT20(2009),
    /**
     * 角色名称长度超长度过20
     */
    ROLE_NAME_SIZE_OUT20(2010),
    ;
    private final int i;
    RoleCode(int i) {
        this.i = i;
    }
    @Override
    public int value() {
        return 0;
    }
}
