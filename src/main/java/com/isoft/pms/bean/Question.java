package com.isoft.pms.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

/**
 * @author shangzhiwei
 * 题目实体类
 */

@ApiModel(value = "题目信息")
@Data
public class Question {

    @ApiModelProperty(value = "题目编号")
    private Long id;
    @ApiModelProperty(value = "上传者编号")
    private Long userId;
    @ApiModelProperty(value = "题目提干")
    private String questionContent;
    @ApiModelProperty(value = "题目类型")
    private Integer questionType;
    @ApiModelProperty(value = "题目答案")
    private String answer;
    @ApiModelProperty(value = "题目分数")
    private Integer score;
    @ApiModelProperty(value = "题目难易程度")
    private String level;
    @ApiModelProperty(value = "专业编号")
    private Long majorId;
    @ApiModelProperty(value = "题目状态")
    private Integer status;
    @ApiModelProperty(value = "题目是否为客观题")
    private Integer isObjective;
    @ApiModelProperty(value = "题目备注")
    private String remark;
}
