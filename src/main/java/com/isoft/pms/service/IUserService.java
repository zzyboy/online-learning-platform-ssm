package com.isoft.pms.service;
/**
 * @author huxiongwei
 */
import com.isoft.pms.bean.Role;
import com.isoft.pms.bean.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface IUserService {

    /**
     * 新增用户
     * @param user 用户实体信息
     * @return 操作成功返回true 操作失败返回false
     */
    boolean addUser(User user);
    /**
     * 更新用户信息
     * @param user 用户实体
     * @return 操作成功返回true 操作失败返回false
     */
    boolean updateUser(User user);
    /**
     * 根据id，物理删除用户
     * @param id 用户id
     * @return 操作成功返回true 操作失败返回false
     */
    boolean delUser(Long id);
    /**
     * 根据条件模糊查询用户
     * @param user 条件
     * @return 符合条件的用户列表
     */
    List<User> getUser(User user);

    /**
     * 逻辑删除用（更改status为1）
     * @param id 用户的id
     * @return 操作成功返回1 操作失败返回0
     */
    boolean fakeDeleteUser(Long id);

    /**
     *  查询所有的用户信息
     * @return 所有的用户列表
     */
    List<User> getAllUser();

    /**
     * 根据用户编号判断是否在数据库中
     * @param id 用户编号
     * @return true在数据库中，false不在数据库中
     */
    boolean userIsExitById(Long id);

    /**
     *
     * @param username 用户名
     * @param password 用户密码
     * @return 符合返回true 不符合返回false
     */
    boolean userLogin(String username,String password);

    /**
     *  通过用户名获得角色信息
     * @param username 用户名
     * @return 角色
     */
    Role userByUserName(String username);

    /**
     * 修改某个用户状态
     * @param user 必须含有状态和用户id
     * @return
     */
    boolean updateUserStatus(User user);

}
