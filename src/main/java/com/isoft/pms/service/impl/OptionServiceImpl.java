package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Option;
import com.isoft.pms.dao.IOptionDao;
import com.isoft.pms.exception.OptionServiceException;
import com.isoft.pms.service.IOptionService;
import com.isoft.pms.unit.OptionCode;
import com.isoft.pms.unit.StringUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


/**
 * @author shangzhiwei
 */

@Service
public class OptionServiceImpl implements IOptionService {

    @Autowired
    private IOptionDao dao;

    @Override
    public boolean addOption(Option option) {
        if(StringUnit.isNull(option.getOptionContent())){
            throw new OptionServiceException(OptionCode.OPTION_CONTENT_ISNULL);
        }else if(StringUnit.isNull(option.getStatus())){
            throw new OptionServiceException(OptionCode.OPTION_STATUS_ISNULL);
        }else if(option.getStatus() != 0 && option.getStatus() != 1 && option.getStatus() != 2){
            throw new OptionServiceException(OptionCode.OPTION_STATUS_NOT_EXIST);
        }
        return dao.addOption(option);
    }

    @Override
    public List<Option> selectAllOption() { return dao.selectOptions(new Option()); }

    @Override
    public boolean deleteOption(Long id) {
        return dao.deleteOption(id);
    }

    @Override
    public boolean updateOption(Option option) {
        return dao.updateOption(option);
    }

    @Override
    public boolean fakeDeleteOption(Long id) {
        return dao.fakeDeleteOption(id);
    }

    @Override
    public List<Option> selectOption(Option option) {
        return dao.selectOptions(option);
    }

}
