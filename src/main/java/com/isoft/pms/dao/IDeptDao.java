package com.isoft.pms.dao;

import com.isoft.pms.bean.Dept;
import org.mapstruct.Mapper;

import java.util.List;


/** 部门mapper层
 * @author zhouzhenyuan
 */
@Mapper
public interface IDeptDao {

    /**
     * 获得部门信息
     * @param dept
     * @return
     */
    List<Dept> getDept(Dept dept);

    /**
     * 添加部门信息
     * @param dept
     * @return
     */
    boolean addDept(Dept dept);

    /**
     * 根据id删除部门信息
     * @param id
     * @return
     */
    boolean deleteDept(Long id);

    /**
     *  更新部门信息 必须带有主键
     * @param dept
     * @return
     */
    boolean updateDept(Dept dept);

}
