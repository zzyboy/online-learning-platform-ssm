package com.isoft.pms.dao;


import com.isoft.pms.bean.Student;
import com.isoft.pms.dao.sql.StudentSQL;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author gaixiankang
 */
public interface IStudentDao {

    /**
     * 添加学生
     * @param student
     * @return
     */
    @Insert("insert into student(user_id,student_id,student_name,student_sex,major_id,status) values(#{userId},#{studentId},#{studentName},#{studentSex},#{majorId},#{status})")
    boolean addStudent(Student student);

    /**
     * 添加学生的时候，查看用户id重复
     * @param userId
     * @return
     */
    @Select("select * from student where user_id=#{userId}")
    Student selectByUserId(Long userId);

    /**
     * 根据id删除学生
     * @param id
     * @return
     */
    @Delete("delete from student where id=#{id}")
    boolean deleteById(@Param("id") Long id);

    /**
     * 根据id更新学生
     * @param student
     * @return
     */
    @UpdateProvider(type = StudentSQL.class,method = "createUpdateStudentSQL")
    boolean updateById(Student student);

    /**
     * 查询所有学生
     * @param student
     * @return
     */
    List<Student> selectStudents(Student student);

    /**
     * 根据id逻辑删除
     * @param id
     * @return
     */
    @Update("update student set status=1 where id=#{id}")
    boolean fakeDeleteStudent(Long id);
}
