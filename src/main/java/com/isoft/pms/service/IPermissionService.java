package com.isoft.pms.service;


import com.isoft.pms.bean.Permission;

import java.util.List;

public interface IPermissionService {

    /**
     *  获得所有权限列表
     * @return 权限列表
     */
    List<Permission> getAllPermission();

    /**
     *  插入权限接口
     * @param permission 要插入的权限 主键可以为空
     * @return 操作成功返回true 操作失败返回false
     */
    boolean addPermission(Permission permission);

    /**
     *  根据id删除权限信息
     * @param id
     * @return 操作成功返回true 操作失败返回false
     */
    boolean deletePermission(Long id);

    /**
     *  根据id更新权限接口
     * @param permission
     * @return
     */
    boolean updatePermission(Permission permission);

    /**
     *  模糊搜索权限接口
     * @param permission 条件
     * @return 权限实体类
     */
    List<Permission> getPermission(Permission permission);
}
