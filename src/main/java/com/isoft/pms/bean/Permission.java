package com.isoft.pms.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhouzhenyuan
 */
@Data
@ApiModel(value = "权限信息")
public class Permission {

    @ApiModelProperty(value = "权限编号")
    private Long id;

    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    @ApiModelProperty(value = "权限url")
    private String url;

    @ApiModelProperty(value = "权限描述")
    private String description;

    @ApiModelProperty(value = "状态")
    private Integer status;
}
