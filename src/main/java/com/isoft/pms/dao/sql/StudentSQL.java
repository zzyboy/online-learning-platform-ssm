package com.isoft.pms.dao.sql;

import com.isoft.pms.bean.Student;
import org.apache.ibatis.jdbc.SQL;

/**
 * @author gaixiankang
 */
public class StudentSQL {

    public String createUpdateStudentSQL(final Student student){
        return new SQL(){{
            UPDATE("student");
            if(student.getUserId()!=0){
                SET("user_id="+student.getUserId());
            }
            if(student.getStudentId()!=0){
                SET("student_id="+student.getStudentId());
            }if(student.getStudentSex()!=0){
            if(student.getStudentName()!=null){
                SET("student_name=#{studentName}");
            }

                SET("student_sex="+student.getStudentSex());
            }
            if(student.getMajorId()!=0){
                SET("major_id="+student.getMajorId());
            }
            if(student.getStatus()!=0){
                SET("status="+student.getStatus());
            }
            WHERE("id="+student.getId());
        }}.toString();
    }

}
