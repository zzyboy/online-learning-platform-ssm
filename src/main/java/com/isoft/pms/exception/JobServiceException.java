package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.JobCode;

/**
 * 任务业务异常类
 * @author shangzhiwei
 */
public class JobServiceException extends BaseServiceException{

    public JobServiceException(JobCode code){
        super(code);
        putIt(JobCode.JOB_CREATE_ID_ISNULL,"创建者编号为空").
                putIt(JobCode.JOB_CREATE_ID_NOT_EXIST,"创建者编号不存在").
                putIt(JobCode.JOB_TYPE_ISNULL,"任务类型为空").
                putIt(JobCode.JOB_TYPE_NOT_EXIST,"任务类型不存在").
                putIt(JobCode.JOB_START_TIME_ISNULL,"任务开始时间为空").
                putIt(JobCode.JOB_START_TIME_LESS_CREATE_TIME,"任务开始时间比任务创造时间早").
                putIt(JobCode.JOB_END_TIME_ISNULL,"任务结束时间为空").
                putIt(JobCode.JOB_END_TIME_LESS_START_TIME,"任务结束时间比任务开始时间早").
                putIt(JobCode.JOB_STATUS_ISNULL,"状态码为空").
                putIt(JobCode.JOB_STATUS_NOT_EXIST,"状态码不存在").
                putIt(JobCode.JOB_DEPT_ID_NOT_EXIST,"部门编号不存在").
                putIt(JobCode.JOB_REMARK_OUT250,"备注超过250字").
                putIt(JobCode.JOB_SCORE_LESSZERO,"任务总分小于等于零");
    }
}
