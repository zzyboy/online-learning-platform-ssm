import request from '@/unit/request'

export function getAllDept() {
    return request({
        url: '/dept/dept-getAll',
        method: 'get'
    });
}
export function getDept(data) {
    return request({
        url:'/dept/dept-get',
        method:'post',
        data
    })
}
export function addDept(data) {
    return request({
        url:'/dept/dept-add',
        method: 'put',
        data
    })
}

export function fakeDeleteDept(id) {
    return request({
        url:`/dept/dept-fakeDelete?id=${id}`,
        method:'delete'
    })
}

export function editDept(dept){
    return request({
        url:'/dept/dept-update',
        method:'post',
        dept
    })
}


export function deptChildRecursion (dept) {
    dept.forEach((item) =>{
            item['children'] = []
    })
    dept.forEach((item,index,array) => {
        if (item.parentId !== null && item.status !== 1) {
            array.forEach((item1) => {
                if (item1.id === item.parentId) {
                    item1.children.push(item)
                }
            })
        }
    })
    dept.length = 1;
    return dept;
}

