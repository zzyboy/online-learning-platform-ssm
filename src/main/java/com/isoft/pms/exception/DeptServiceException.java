package com.isoft.pms.exception;

import com.isoft.pms.bean.Dept;
import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.DeptCode;

/** 部门业务异常类
 * @author zhouzhenyuan
 */
public class DeptServiceException extends BaseServiceException {

    public DeptServiceException(DeptCode code) {
        super(code);
        putIt(DeptCode.DEPT_NAME_ISNULL,"部门名称为空")
                .putIt(DeptCode.DEPT_ID_ISNULL,"部门编号为空")
                .putIt(DeptCode.DEPT_PARENT_ID_ISNULL,"父部门编号为空")
                .putIt(DeptCode.DEPT_LEADER_ID_ISNULL,"领导用户编号为空")
                .putIt(DeptCode.DEPT_STATUS_ISNULL,"状态码为空")
                .putIt(DeptCode.DEPT_NAME_SIZE_OUT50,"部门名称不能超过50字")
                .putIt(DeptCode.DEPT_PARENT_ID_NOT_EXIST,"父部门编号不存在")
                .putIt(DeptCode.DEPT_LEADER_ID_NOT_EXIST,"领导所在用户编号不存在")
                .putIt(DeptCode.DEPT_STATUS_NOT_EXIST,"状态码不存在")
                .putIt(DeptCode.DEPT_REMARKS_SIZE_OUT200,"备注信息不能超过200字")
                .putIt(DeptCode.DEPT_ID_IS_EXIST,"部门编号已存在")
                .putIt(DeptCode.DEPT_ID_IS_NOT_EXIST,"部门编号不存在");
    }
}
