package com.isoft.pms.controller;

import com.isoft.pms.unit.AjaxResult;
import com.sun.management.OperatingSystemMXBean;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.*;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author zhouzhenyuan
 */
@RestController
@RequestMapping("/system")
public class SysController {

    @GetMapping("/info")
    public AjaxResult getInfo() throws UnknownHostException {
        SystemInfo systemInfo = new SystemInfo();
        return AjaxResult.success("获得成功",systemInfo);
    }

    public static void main(String[] args) throws UnknownHostException {
        SystemInfo s = new SystemInfo();
        System.out.println(s);
    }
    @Data
    private static class SystemInfo{
        SystemInfo() throws UnknownHostException {
            String ip = null;
            DecimalFormat df   = new DecimalFormat("######0.00");
            OperatingSystemMXBean osmxb = (OperatingSystemMXBean) ManagementFactory
                    .getOperatingSystemMXBean();
            double t = osmxb.getTotalPhysicalMemorySize() / 1024/1024;
            InetAddress addr = InetAddress.getLocalHost();
            String html = httpGet("https://www.baidu.com/s?wd=ip");

            // 提出IP

            Pattern pattern = Pattern.compile("<span\\sclass=\"c-gap-right\">本机IP:&nbsp;([^<]+)</span>");

            Matcher matcher = pattern.matcher(html);

            if (matcher.find()) {

                 ip = matcher.group(1);

            }


            this.ip =ip;
            this.sysName = addr.getHostName();
            this.coreNum = Runtime.getRuntime().availableProcessors();
            this.sysType =  System.getProperty("os.name");
            this.sysMemory = df.format(t/1024);
        }
        private int coreNum;
        private String using;
        private String sysFree;

        private String sysMemory;
        private String jvmMemory;
        private String useSysMemory;
        private String useJvmMemory;

        private String sysName;
        private String ip;
        private String sysType;

        private String javaPath;
        private String projectPath;
        private String createTime;
        private String jdkType;

    }
    // 获取网页源码
    static String httpGet(String url) {

        StringBuffer buffer = new StringBuffer();

        try {

            URLConnection conn = new URL(url).openConnection();

            conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");

            try (InputStream inputStream = conn.getInputStream();
                 InputStreamReader streamReader = new InputStreamReader(inputStream);
                 BufferedReader reader = new BufferedReader(streamReader);) {

                String line = null;

                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append(System.lineSeparator());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }
}
