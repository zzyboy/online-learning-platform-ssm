package com.isoft.pms.controller;

import com.isoft.pms.bean.Student;
import com.isoft.pms.service.IStudentService;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @author gaixiankang
 */
@Controller
@RequestMapping("/student")
@CrossOrigin(maxAge = 3600)
@Api(description = "学生管理测试接口")
public class StudentController {

    @Autowired
    private IStudentService service;

    @ApiOperation(value = "添加学生测试")
    @ResponseBody
    @PutMapping("/student-add")
    public AjaxResult add(@RequestBody Student student){
        return AjaxResult.success("添加成功",service.addStudent(student));
    }

    @ApiOperation(value = "删除学生测试")
    @ResponseBody
    @DeleteMapping("/student-deleteById")
    public AjaxResult delete(Long id){
        return AjaxResult.success("删除成功",service.deleteStudent(id));
    }

    @ApiOperation(value = "修改学生测试")
    @ResponseBody
    @PostMapping("/student-updateById")
    public AjaxResult update(@RequestBody Student student){
        return AjaxResult.success("更新成功",service.updateStudent(student));
    }

    @ApiOperation(value = "查看所有学生测试")
    @ResponseBody
    @GetMapping("/student-selectAllStudent")
    public AjaxResult selectAllStudent() {
        return AjaxResult.success("查询成功",service.selectAllStudent());
    }

    @ApiOperation(value="根据条件模糊查询学生信息")
    @ResponseBody
    @GetMapping("/student-selectStudent")
    public AjaxResult selectStudent(@RequestBody Student student) {
        return AjaxResult.success("查询成功",service.selectStudent(student));
    }

    @ApiOperation(value = "根据学生编号逻辑删除学生信息")
    @ResponseBody
    @DeleteMapping("/student-fakeDeleteById")
    public AjaxResult fakeDeleteStudent(Long id){
        return AjaxResult.success("逻辑删除成功",service.fakeDeleteStudent(id));
    }
}
