package com.isoft.pms.controller;

import com.isoft.pms.bean.Option;
import com.isoft.pms.service.IOptionService;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

/**
 * @author shangzhiwei
 */
@RestController
@RequestMapping("/option")
@CrossOrigin(maxAge = 3600)
@Api(description = "选项管理接口")
public class OptionController {

    @Autowired
    private IOptionService service;

    @ApiOperation(value = "添加选项")
    @PutMapping( "/option-add")
    public AjaxResult addOption(@RequestBody Option option){
        return AjaxResult.success("添加成功",service.addOption(option));
    }

    @ApiOperation(value= "查询所有选项信息")
    @GetMapping("/option-selectAllOption")
    public AjaxResult selectAllOption() { return AjaxResult.success("查询成功",service.selectAllOption()); }

    @ApiOperation(value = "删除选项")
    @DeleteMapping("/option-delete")
    public AjaxResult deleteOption(Long id) {
        return AjaxResult.success("删除成功",service.deleteOption(id));
    }

    @ApiOperation(value = "修改选项")
    @PostMapping("/option-update")
    public AjaxResult updateOption(@RequestBody Option option) {
        return AjaxResult.success("修改成功",service.updateOption(option));
    }

    @ApiOperation(value = "根据选项编号逻辑删除选项")
    @ResponseBody
    @DeleteMapping("/option-fakeDeleteById")
    public AjaxResult fakeDeleteOption(Long id){ return AjaxResult.success("逻辑删除成功",service.fakeDeleteOption(id)); }

    @ApiOperation(value="根据条件模糊查询选项信息")
    @ResponseBody
    @GetMapping("/option-selectOption")
    public AjaxResult selectOption(@RequestBody Option option) {
        return AjaxResult.success("查询成功", service.selectOption(option));
    }
}
