package com.isoft.pms.service;

import com.isoft.pms.bean.User;

public interface ILoginService {

    /**
     *
     * @return
     */
    boolean userLogin(User user);
}
