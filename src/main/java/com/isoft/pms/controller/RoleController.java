package com.isoft.pms.controller;

import com.isoft.pms.bean.Role;
import com.isoft.pms.service.IRoleService;
import com.isoft.pms.service.impl.RoleServiceImpl;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhouzhenyuan
 */
@RestController
@RequestMapping("/role")
@Api(description = "权限管理测试接口")
@CrossOrigin(maxAge = 3600)
public class RoleController {

    @Autowired
    IRoleService roleService = new RoleServiceImpl();

    @ApiOperation(value = "添加角色")
    @PutMapping ("/role-add")
    public AjaxResult add(@RequestBody Role role) {
        return AjaxResult.success("添加成功",roleService.addRole(role));
    }

    @ApiOperation(value= "获得全部角色信息")
    @GetMapping("/role-getAll")
    public AjaxResult getAll() {
        return AjaxResult.success("获得成功",roleService.getAllRole());
    }

    @ApiOperation(value = "根据角色编号更新角色")
    @PostMapping("/role-updateById")
    public AjaxResult updateById(Role role) {
        return AjaxResult.success("更新成功",roleService.updateRole(role));
    }

    @ApiOperation(value = "根据角色编号逻辑删除角色信息")
    @DeleteMapping("/role-fakeDeleteById")
    public AjaxResult fakeDeleteById(Long id) {
        return AjaxResult.success("删除成功",roleService.fakeDeleteRole(id));
    }

    @ApiOperation(value = "根据角色编号物理删除角色信息")
    @DeleteMapping("/role-deleteById")
    public AjaxResult deleteById(Long id) {
        return AjaxResult.success("删除成功",roleService.deleteRole(id));
    }

    @ApiOperation(value= "按条件模糊搜索角色")
    @PostMapping("/role-get")
    public AjaxResult get(Role role) {
        return AjaxResult.success("查询成功",roleService.getRole(role));
    }

    @ApiOperation(value="为角色添加权限")
    @PutMapping("/role-addPermission")
    public AjaxResult addPermission(Long permissionId,Long roleId) {
        return AjaxResult.success("添加成功",roleService.addPermissionWithRole(permissionId,roleId));
    }

    @ApiOperation(value="通过角色id获得角色信息")
    @GetMapping("/role-getRoles/{roleId}")
    public AjaxResult getRoleById (@PathVariable Long roleId) {
        return AjaxResult.success("获取成功",roleService.getRoleById(roleId));
    }

}
