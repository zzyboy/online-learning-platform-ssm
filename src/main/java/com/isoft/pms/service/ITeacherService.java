package com.isoft.pms.service;

import com.isoft.pms.bean.Teacher;

import java.util.List;
import java.util.Map;

/**
 * @author gaixiankang
 * service接口
 */
public interface ITeacherService {

    /**
     * 添加教师接口
     * @param teacher
     * @return
     */
    boolean addTeacher(Teacher teacher);

    /**
     * 根据id删除教师接口
     * @param id
     * @return
     */
    boolean deleteTeacher(Long id);

    /**
     * 动态更新教师接口
     * @param teacher
     * @return
     */
    boolean updateTeacher(Teacher teacher);

    /**
     * 查询所有教师接口
     * @return
     */
    List<Teacher> selectAllTeacher();

    /**
     * 模糊查询教师接口
     * @param teacher
     * @return
     */
    List<Teacher> selectTeacher(Teacher teacher);

    /**
     * 根据id逻辑删除老师
     * @param id
     * @return 操作成功返回true 操作失败返回 false
     */
    boolean fakeDeleteTeacher(Long id);

}
