import {createWebHistory, createRouter} from 'vue-router'
import store from '@/store'

const router = new createRouter({
    history: createWebHistory(),
    routes: [
        {
            //路径
            path: '/',
            //重定向到
            redirect: '/home',
            hidden: true
        },
        {
            path: '/login',
            component: () => import('@/view/LoginPage'),
            hidden: true

        },
        {
            path: '/register',
            component: () => import('@/view/RegisterPage'),
            hidden: true
        },
        {
            path: '/home',
            name: 'home',
            hidden: true,
            component: () => import('@/view/home/StudentPlatform'),
            children:[
                {
                    path:'',
                    component: import('@/view/home/StudentHome'),
                    hidden:true,
                    name:'studentHome'
                },
                {
                    path:'learning',
                    component: import('@/view/home/Learning'),
                    hidden:true,
                    name:'learning'
                },
                {
                    path:'studentSpace',
                    component: import('@/view/home/StudentSpace'),
                    hidden:true,
                    name:'studentSpace'
                }
            ]
        },
        {
            path: '/admin',
            component: () => import('@/view/AdminPage'),
            hidden: true,
            children: [
                {
                    path: 'platform/user',
                    component: () => import('@/view/user/PersonalManage'),
                    hidden: true
                },
                {
                    name: 'admin',
                    path: '',
                    hidden: true,
                    redirect: '/admin/home',
                },
                {
                    name: 'adminHome',
                    path: 'home',
                    hidden: true,
                    component: () => import('@/view/HomePage')
                },
                {
                    name: 'role',
                    path: 'platform/role',
                    hidden: true,
                    component: () => import('@/view/role/RolePage')
                }, {
                    name: 'permission',
                    path: 'platform/permission',
                    hidden: true,
                    component: () => import('@/view/permission/PermissionPage')
                },
                {
                    name: 'teacher',
                    path: 'platform/teacher',
                    hidden: true,
                    component: () => import('@/view/teacher/TeacherPage')
                },
                {
                    name: 'question',
                    path: 'platform/question',
                    hidden: true,
                    component: () => import('@/view/question/QuestionPage')
                },
                {
                    name:'jobEditPage',
                    path: 'platform/jobEditPage/:jobId/:jobType',
                    component: () => import('@/view/job/JobEditPage'),
                    props: true
                },
                {
                    name: 'dept',
                    path: 'platform/dept',
                    hidden: true,
                    component: () => import('@/view/dept/DeptPage')
                }, {
                    name: 'role',
                    path: 'platform/role',
                    hidden: true,
                    component: () => import('@/view/role/RolePage')
                }, {
                    name: 'student',
                    path: 'platform/student',
                    hidden: true,
                    component: () => import('@/view/student/StudentPage')
                },
                {
                    name: 'option',
                    path: 'platform/option',
                    hidden: true,
                    component: () => import('@/view/option/OptionPage')
                },
                {
                    name: 'job',
                    path: 'platform/job',
                    hidden: true,
                    component: () => import('@/view/job/JobPage')
                }, {
                    name: 'api',
                    path: 'api',
                    hidden: true,
                    component: () => import('@/view/system/SystemApi')
                }
            ]
        },
        {path: '/404', component: () => import('@/view/error/404'), hidden: true},
        {path: '/:pathMatch(.*)', redirect: '/404', hidden: true}
    ]
})
router.beforeEach((to, from, next) => {
    const pathArray = to.path.split('/');
    if (sessionStorage.getItem('token') == null && pathArray[1] !== 'login') {
        router.push('/login').then(r => {
            console.log(r)
        })
    }
    //输出检测route对象 检测非根目录的路径
    if (pathArray.length > 1) {
        if (pathArray[1] === 'admin') {
            store.commit('editBread', to.path)
            if (pathArray.slice(-1)[0] !== 'home') {
                store.commit('addTag', {
                    name: pathArray.slice(-1)[0],
                    path: to.path
                })
            }
        }
        if (pathArray[1] === 'login') {
            if (sessionStorage.getItem('token') !== null) {
                router.push('/admin').then(r => {
                    console.log(r)
                })
            }
        }
    }
    // console.log(to.path.split('/'))
    next();
})


export default router;