package com.isoft.pms.service.impl;
/**
 * @author huxiongwei
 */
import com.isoft.pms.bean.Role;
import com.isoft.pms.bean.User;
import com.isoft.pms.dao.IUserDao;
import com.isoft.pms.exception.UserServiceException;
import com.isoft.pms.service.IUserService;
import com.isoft.pms.unit.PassWordUtil;
import com.isoft.pms.unit.Status;
import com.isoft.pms.unit.StringUnit;
import com.isoft.pms.unit.UserCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserDao dao;

    //用户注册
    @Override
    public boolean addUser(User user) {
        user.setSalt(PassWordUtil.createSalt());
        user.setPassword(PassWordUtil.encryption(user.getPassword(),user.getSalt(),3));
        //查询用户名是否存在
//        User user2=dao.getUser(user.getUsername());
        User user1= dao.selectByName(user.getUsername());
        if (user1!=null){
            throw new UserServiceException(UserCode.USER_NAME_EXIST);
        }
        else if(StringUnit.isNull(user.getUsername())){
            throw new UserServiceException(UserCode.USER_NAME_ISNULL);
        }else if (StringUnit.isNull(user.getPassword())){
            throw new UserServiceException(UserCode.USER_PASSWORD_ISNULL);
        }else if (StringUnit.isNull(user.getSalt())){
            throw new UserServiceException(UserCode.USER_SALT_ISNULL);
        }else if (StringUnit.isNull(user.getCreater())){
            throw new UserServiceException(UserCode.USER_CREATER_ISNULL);
        }else if (StringUnit.isNull(user.getStatus())){
            throw new UserServiceException(UserCode.USER_STATUS_ISNULL);
        }else if (StringUnit.isNull(user.getRoleId())){
            throw new UserServiceException(UserCode.USER_ROLE_ID_ISNULL);
        }else if (StringUnit.isNull(user.getDeptId())){
            throw new UserServiceException(UserCode.USER_DEPT_ID_ISNULL);
        }
        return dao.addUser(user);
    }

    @Override
    //更改用户信息
    public boolean updateUser(User user) {

        return dao.updateUser(user);
    }

    @Override
    public boolean delUser(Long id) {
        User user = new User();
        user.setId(id);
        for (User user1:dao.getUser(user)){
            if (user1.getId().equals(id)){
                return dao.deleteUser(id);
            }
        }
        return false;
    }


    @Override
    public List<User> getUser(User user) {

        return dao.getUser(user);
    }

    @Override
    public boolean fakeDeleteUser(Long id) {

        User user = new User();
        user.setId(id);
        for (User user1:dao.getUser(user)){
            if (user1.getId().equals(id)){
                user.setStatus(Status.DELETE.value());
                return dao.updateUser(user);
            }
        }
        return false;


    }

    @Override
    public List<User> getAllUser() {
        return dao.getUser(new User());
    }

    @Override
    public boolean userIsExitById(Long id) {
        User user = new User();
        user.setId(id);
        for (User user1:dao.getUser(user)){
            if (user1.getId().equals(id)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean userLogin(String username, String password) {
        User user = new User();
        user.setUsername(username);
        for (User user1:this.getUser(user)) {
            if (username.equals(user1.getUsername())) {
                if (PassWordUtil.isRealPass(password,user1.getPassword(),user1.getSalt(),3)) {
                    return true;
                } else {
                    throw new UserServiceException(UserCode.USER_PASSWORD_ERROR);
                }
            } else {
                throw new UserServiceException(UserCode.USER_NAME_ERROR);
            }
        }
        throw new UserServiceException(UserCode.USER_NAME_ERROR);
    }

    @Override
    public Role userByUserName(String username) {
        return null;
    }

    @Override
    public boolean updateUserStatus(User user) {
        if (StringUnit.isNull(user.getId())) {
            throw new UserServiceException(UserCode.USER_ID_ISNULL);
        } else if (StringUnit.isNull(user.getStatus())) {
            throw new UserServiceException(UserCode.USER_STATUS_ISNULL);
        } else if (user.getStatus()<0||user.getStatus()>2) {
            throw new UserServiceException(UserCode.USER_STATUS_NOT_EXIST);
        }

        return dao.updateUser(user);
    }


}
