package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.TeacherCode;

/**
 * 教师业务异常类
 * @author gaixiankang
 */
public class TeacherServiceException extends BaseServiceException {


    public TeacherServiceException(TeacherCode code) {
        super(code);
        putIt(TeacherCode.TEACHER_ID_ISNULL,"教师编号为空").
                putIt(TeacherCode.TEACHER_NAME_ISNULL,"教师姓名为空").
                putIt(TeacherCode.TEACHER_SEX_ISNULL,"教师性别为空").
                putIt(TeacherCode.TEACHER_STATUS_ISNULL,"状态码为空").
                putIt(TeacherCode.TEACHER_USER_ID_ISNULL,"用户编号为空").
                putIt(TeacherCode.TEACHER_ID_ALREADY_EXIST,"教师编号已存在").
                putIt(TeacherCode.TEACHER_NAME_SIZE_OUT50,"教师姓名超过50字").
                putIt(TeacherCode.TEACHER_STATUS_NOT_EXIST,"教师状态码不存在").
                putIt(TeacherCode.TEACHER_USER_ID_NOT_EXIST,"用户编号不存在").
                putIt(TeacherCode.TEACHER_SEX_NOT_EXIST,"教师性别不存在").
                putIt(TeacherCode.TEACHER_USER_ID_ALREADY_EXIST,"该用户已被其他角色占用");
    }
}
