package com.isoft.pms.service;

import com.isoft.pms.bean.Role;

import java.util.List;

/**
 * @author zhouzhenyuan
 */

public interface IRoleService {
    /**
     *  添加用户业务接口
     * @param role
     * @return
     */
    boolean addRole(Role role);

    /**
     *  获得所有的角色信息
     * @return
     */
    List<Role> getAllRole();

    /**
     * 模糊查询角色
     *
     */
    List<Role> getRole(Role role);

    /**
     *  更新角色信息 需要有主键
     * @param role
     * @return
     */
    boolean updateRole(Role role);

    /**
     *  根据id逻辑删除角色信息
     * @param id
     * @return
     */
    boolean fakeDeleteRole(Long id);

    /**
     *  根据id物理删除角色信息
     * @param id
     */
    boolean deleteRole(Long id);

    boolean addPermissionWithRole(Long permissionId,Long RoleId);

    /**
     * 精准查询角色信息
     * @param role
     * @return
     */
    List<Role> getRoles(Role role);

    Role getRoleById(Long id);
}
