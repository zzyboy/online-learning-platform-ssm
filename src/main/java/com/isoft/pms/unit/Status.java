package com.isoft.pms.unit;

/** 状态枚举类
 * @author zhouzhenyuan
 */

public enum Status {

    NORMAL(0),
    DELETE(1),
    STOP(2);
    private final int value;
    Status(Integer value) {this.value = value;}
    Status() {this.value = 0;}
    public Integer value() {return this.value;}
}
