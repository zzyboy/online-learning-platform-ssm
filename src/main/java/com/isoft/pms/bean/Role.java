package com.isoft.pms.bean;

import com.isoft.pms.unit.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhouzhenyuan
 * 角色实体类
 */
@ApiModel(value = "角色信息")
@Data
public class Role {

    @ApiModelProperty(value = "角色编号")
    private Long id;
    @ApiModelProperty(value = "角色标识符")
    private String roleKey;
    @ApiModelProperty(value = "角色名字")
    private String roleName;
    @ApiModelProperty(value = "角色描述")
    private String description;
    @ApiModelProperty(value = "状态")
    private Integer status;


}
