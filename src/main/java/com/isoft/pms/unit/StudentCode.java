package com.isoft.pms.unit;

/**
 * 状态码枚举类
 * @author gaixiankang
 */
public enum  StudentCode implements BaseCode{

    /**
     * 状态码
     */
    STUDENT_USER_ID_ISNULL(5001),
    STUDENT_ID_ISNULL(5002),
    STUDENT_NAME_ISNULL(5003),
    STUDENT_SEX_ISNULL(5004),
    STUDENT_MAJOR_ID_ISNULL(5005),
    STUDENT_STATUS_ISNULL(5006),
    STUDENT_USER_ID_NOT_EXIST(5007),
    STUDENT_NAME_SIZE_OUT50(5008),
    STUDENT_MAJOR_ID_NOT_EXIST(5009),
    STUDENT_STATUS_NOT_EXIST(5010),
    STUDENT_ID_ALREADY_EXIST(5011),
    STUDENT_SEX_NOT_EXIST(5012),
    STUDENT_USER_ID_ALREADY_EXIST(5013),
    ;
    private final int i;
    StudentCode(int i) {
        this.i = i;
    }
    @Override
    public int value() {
        return i;
    }
}
