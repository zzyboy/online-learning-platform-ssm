package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.RoleCode;

/**
 * @author zhouzhenyuan
 */
public class RoleServiceException extends BaseServiceException {
    public RoleServiceException(RoleCode code) {
        super(code);
        putIt(RoleCode.ROLE_KEY_IS_EXIT,"角色标识符已存在")
                .putIt(RoleCode.ROLE_KEY_IS_NOT_EXIT,"角色标识符不存在")
                .putIt(RoleCode.ROLE_ID_ISNULL,"角色编号为空")
                .putIt(RoleCode.ROLE_STATUS_ISNULL,"角色状态码为空")
                .putIt(RoleCode.ROLE_STATUS_IS_NOT_EXIT,"角色状态码异常")
                .putIt(RoleCode.ROLE_ID_IS_EXIT,"角色编号已存在")
                .putIt(RoleCode.ROLE_DESCRIPTION_SIZE_OUT50,"角色描述信息长度超出50")
                .putIt(RoleCode.ROLE_KEY_SIZE_OUT20,"角色标识符长度超出20")
                .putIt(RoleCode.ROLE_NAME_SIZE_OUT20,"角色名称长度超出20");
    }
}
