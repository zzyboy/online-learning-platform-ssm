package com.isoft.pms.dao.sql;
/**
 * @author huxiongwei
 */
import com.isoft.pms.bean.User;
import org.apache.ibatis.jdbc.SQL;

public class UserSQL {
    public String createUpdateUserSQL(final User user){
        return new SQL(){{
            UPDATE("user");
            if (user.getId()!=0){
                SET("id="+user.getId());
            }
            if (user.getUsername()!=null){
                SET("username=#{username}");
            }
            if (user.getPassword()!=null){
                SET("password=#{password}");
            }
            if (user.getStatus()!=0){
                SET("status="+user.getStatus());
            }
            if (user.getPhoto()!=null){
                SET("photo=#{photo}");
            }WHERE("id="+user.getId());
        }}.toString();
    }
}
