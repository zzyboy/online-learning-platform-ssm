package com.isoft.pms.service;


import com.isoft.pms.bean.Job;
import java.util.List;


/**
 * @author shangzhiwei
 */

public interface IJobService {

    /**
     * 添加任务接口
     * @param job
     * @return
     */
    boolean addJob(Job job);

    /**
     * 查询所有任务接口
     * @return
     */
    List<Job> selectAllJob();

    /**
     * 根据id删除任务接口
     * @param id
     * @return
     */
    boolean deleteJob(Long id);

    /**
     * 根据id修改任务接口
     * @param job
     * @return
     */
    boolean updateJob(Job job);

    /**
     * 根据id逻辑删除任务接口
     * @param id
     * @return
     */
    boolean fakeDeleteJob(Long id);

    /**
     * 模糊查询任务接口
     * @param job
     * @return
     */
    List<Job> selectJob(Job job);
}
