import {createStore} from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import state from './state'
//创建VueX对象
export default createStore({
    getters,
    actions,
    mutations,
    state
})