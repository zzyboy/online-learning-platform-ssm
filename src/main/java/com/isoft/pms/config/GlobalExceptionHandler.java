package com.isoft.pms.config;


import com.auth0.jwt.exceptions.TokenExpiredException;
import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.AjaxResult;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.sql.SQLException;

/** 全局异常捕获器
 * @author zhouzhenyuan
 */
@RestControllerAdvice
public class GlobalExceptionHandler{

    private String message = "（注：非管理员请勿使用测试工具请求该服务器，需要调用相关接口说明请联系qq:3110106873）";

    @ExceptionHandler(RuntimeException.class)
    public AjaxResult server500(RuntimeException ex){
//        System.err.println(ex.getMessage());
        return AjaxResult.error("系统错误！请联系管理员 "+message);
    }

    /**
     * 业务逻辑异常捕获
     * @param serviceException
     * @return json数据
     */
    @ExceptionHandler(BaseServiceException.class)
    public AjaxResult myServerException(BaseServiceException serviceException) {
        return new AjaxResult(serviceException.getCode(),serviceException.getMessage());
    }
    


    @ExceptionHandler({HttpRequestMethodNotSupportedException.class, HttpMediaTypeNotSupportedException.class,
            HttpMediaTypeNotAcceptableException.class, MissingPathVariableException.class,
            MissingServletRequestParameterException.class, ServletRequestBindingException.class,
            ConversionNotSupportedException.class, TypeMismatchException.class,
            HttpMessageNotReadableException.class, HttpMessageNotWritableException.class,
            MethodArgumentNotValidException.class, MissingServletRequestPartException.class,
            BindException.class, NoHandlerFoundException.class, AsyncRequestTimeoutException.class})
    public AjaxResult server400(Exception ex) throws Exception {
        HttpStatus status;
        if (ex instanceof HttpRequestMethodNotSupportedException) {
            status = HttpStatus.METHOD_NOT_ALLOWED;
            return new AjaxResult(status.value(),"请求方法不允许！"+message);
        } else if (ex instanceof HttpMediaTypeNotSupportedException) {
            status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
            return new AjaxResult(status.value(),"不支持的媒体（数据）类型！"+message);
        } else if (ex instanceof HttpMediaTypeNotAcceptableException) {
            status = HttpStatus.NOT_ACCEPTABLE;
            return new AjaxResult(status.value(),"响应的媒体（数据）类型与期望类型不同！"+message);
        } else if (ex instanceof MissingPathVariableException) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            return AjaxResult.error("系统错误！请联系管理员 "+message);
        } else if (ex instanceof MissingServletRequestParameterException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"传输参数丢失! "+message);
        } else if (ex instanceof ServletRequestBindingException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"错误的请求，请检查请求包信息 "+message);
        } else if (ex instanceof ConversionNotSupportedException) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            return AjaxResult.error("系统错误！请联系管理员 "+message);
        } else if (ex instanceof TypeMismatchException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"数据类型转化错误！请检查请求参数 "+message);
        } else if (ex instanceof HttpMessageNotReadableException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"此资源不希望被读取，请检查请求方式 "+message);
        } else if (ex instanceof HttpMessageNotWritableException) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            return new AjaxResult(status.value(),"此资源不希望被写入，请检查请求方式 "+message);
        } else if (ex instanceof MethodArgumentNotValidException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"错误的请求方式 "+message);
        } else if (ex instanceof MissingServletRequestPartException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"错误的请求方式 "+message);
        } else if (ex instanceof BindException) {
            status = HttpStatus.BAD_REQUEST;
            return new AjaxResult(status.value(),"错误到请求格式 "+message);
        } else if (ex instanceof NoHandlerFoundException) {
            status = HttpStatus.NOT_FOUND;
            return  new AjaxResult(status.value(),"未找到资源"+message);
        } else if (ex instanceof AsyncRequestTimeoutException) {
            status = HttpStatus.SERVICE_UNAVAILABLE;
            return new AjaxResult(status.value(),"服务器暂时不可访问，请联系管理员 "+message);
        } else {
            throw ex;
        }
    }


}
