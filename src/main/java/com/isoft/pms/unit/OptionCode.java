package com.isoft.pms.unit;

/**
 * 状态码枚举类
 * @author shangzhiwei
 */
public enum OptionCode implements BaseCode{
    /**
     * 状态码
     */
    OPTION_CONTENT_ISNULL(8001),
    OPTION_STATUS_ISNULL(8002),
    OPTION_STATUS_NOT_EXIST(8003),
    ;
    private final int i;
    OptionCode(int i) {this.i = i; }
    @Override
    public int value() {return this.i;}
}
