package com.isoft.pms.controller;

import com.isoft.pms.bean.User;
import com.isoft.pms.service.IUserService;
import com.isoft.pms.unit.AjaxResult;
import com.isoft.pms.unit.TokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/** 登录管理器
 * @author zhouzhenyuan huxiongwei
 */
@RestController
@Api(description = "登录接口")
@CrossOrigin(maxAge = 3600)
@RequestMapping("/login")
public class LoginController {
    @Autowired
    IUserService userService;

    @ApiOperation("单点登录测试接口")
    @PostMapping
    public AjaxResult login(@RequestBody User user) {
        if (userService.userLogin(user.getUsername(),user.getPassword())) {
            String token = TokenUtil.sign(user.getUsername());
            return AjaxResult.success("登录成功",token);
        }
        return AjaxResult.error("登录失败,未知原因");
    }
}
