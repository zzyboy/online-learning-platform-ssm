package com.isoft.pms.config;

import com.isoft.pms.exception.UserServiceException;
import com.isoft.pms.unit.TokenUtil;
import com.isoft.pms.unit.UserCode;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/** 登录拦截器
 * @author zhouzhenyuan
 */
@Configuration
public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if("OPTIONS".equals(request.getMethod().toUpperCase())) {
            return true;
        }
        String token = request.getHeader("authorization");
            //执行认证
            if(TokenUtil.verify(token)){
                return true;
            } else {
                throw new UserServiceException(UserCode.USER_NOT_LOGIN);
            }
    }
}
