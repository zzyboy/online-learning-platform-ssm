const getters = {
    isCollapse: state  => state.isCollapse,
    tags: state => state.tags,
    bread: state => state.bread,
    user: state => state.user,
    student:state => state.student,
    teacher:state=>state.teacher,
    themecolor:state=>state.themecolor,
}

export default getters