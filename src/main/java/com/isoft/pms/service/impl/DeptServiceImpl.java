package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Dept;
import com.isoft.pms.dao.IDeptDao;
import com.isoft.pms.exception.DeptServiceException;
import com.isoft.pms.service.IDeptService;
import com.isoft.pms.unit.DeptCode;
import com.isoft.pms.unit.Status;
import com.isoft.pms.unit.StringUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhouzhenyuan
 */
@Service
public class DeptServiceImpl implements IDeptService {

    @Autowired
    IDeptDao deptDao;
    /**
     * 最大部门名称长度
     */
    private final int MAX_DEPT_NAME_SIZE = 50;

    /**
     * 最大备注信息长度
     */
    private final int MAX_DEPT_REMARK_SIZE = 200;

    @Override
    public List<Dept> getAllDept() {
        return deptDao.getDept(new Dept());
    }

    @Override
    public boolean addDept(Dept dept) {
        System.out.println(dept.getDeptName());
        if (StringUnit.isNull(dept.getDeptName())) {

            throw new DeptServiceException(DeptCode.DEPT_NAME_ISNULL);

        } else if (StringUnit.isNull(dept.getLeader())) {

            throw new DeptServiceException(DeptCode.DEPT_LEADER_ID_ISNULL);

        } else if (StringUnit.isNull(dept.getStatus())) {

            dept.setStatus(0);

        } else if (dept.getDeptName().length() >= MAX_DEPT_NAME_SIZE) {

            throw new DeptServiceException(DeptCode.DEPT_NAME_SIZE_OUT50);

        } else if (dept.getRemarks().length() >= MAX_DEPT_REMARK_SIZE) {

            throw new DeptServiceException(DeptCode.DEPT_REMARKS_SIZE_OUT200);

        } else if (!deptIdExit(dept.getParentId())) {

            throw new DeptServiceException(DeptCode.DEPT_PARENT_ID_NOT_EXIST);

        } else if (false) {
            //待修改
            throw new DeptServiceException(DeptCode.DEPT_LEADER_ID_NOT_EXIST);

        } else if (!(dept.getStatus().equals(Status.NORMAL.value())
                || dept.getStatus().equals(Status.DELETE.value())
                || dept.getStatus().equals(Status.STOP.value()))) {

            throw new DeptServiceException(DeptCode.DEPT_STATUS_NOT_EXIST);

        } else if (deptIdExit(dept.getId())) {
            throw new DeptServiceException(DeptCode.DEPT_ID_IS_EXIST);
        }

        return deptDao.addDept(dept);

    }

    @Override
    public boolean fakeDeleteDeptById(Long id) {
        if (StringUnit.isNull(id)) {
            throw new DeptServiceException(DeptCode.DEPT_ID_ISNULL);
        } else if(!deptIdExit(id)) {
            throw new DeptServiceException(DeptCode.DEPT_ID_IS_NOT_EXIST);
        }
        Dept dept = new Dept();
        dept.setId(id);
        dept.setStatus(Status.DELETE.value());
        return deptDao.updateDept(dept);
    }

    @Override
    public boolean deleteDeptById(Long id) {
        if (StringUnit.isNull(id)) {
            throw new DeptServiceException(DeptCode.DEPT_ID_ISNULL);
        } else if(!deptIdExit(id)) {
            throw new DeptServiceException(DeptCode.DEPT_ID_IS_NOT_EXIST);
        }
        return deptDao.deleteDept(id);
    }

    @Override
    public boolean updateDept(Dept dept) {
        return deptDao.updateDept(dept);
    }

    @Override
    public List<Dept> getDept(Dept dept) {

        return deptDao.getDept(dept);
    }

    /**
     * 查看部门id是否存在
     * @param id 部门主键
     * @return
     */

    @Override
    public boolean deptIdExit(Long id) {
        Dept dept = new Dept();
        System.out.println(id);
        dept.setId(id);
        for (Dept dept1 : deptDao.getDept(dept)) {
            if (dept1.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

}
