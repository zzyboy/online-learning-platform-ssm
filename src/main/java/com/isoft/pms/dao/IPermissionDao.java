package com.isoft.pms.dao;

import com.isoft.pms.bean.Permission;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author zhouzhenyuan
 */
@Mapper
public interface IPermissionDao {
    /**
     *  根据条件模糊查询所有权限
     * @param permission
     * @return
     */
    List<Permission> getPermission(Permission permission);

    /**
     *  添加权限信息 可以没有主键信息
     * @param permission
     * @return
     */
    boolean addPermission(Permission permission);

    /**
     * 更新权限，要有主键
     * @param permission
     * @return
     */
    boolean updatePermission(Permission permission);

    /**
     * 根据主键删除权限信息
     * @param id
     * @return
     */
    boolean deletePermission(Long id);

    /**
     *  通过权限id和角色id添加到关系表
     * @param permissionId  权限id
     * @param roleId  角色id
     * @return
     */
    boolean addPermissionWithRole(Long permissionId,Long roleId);
}
