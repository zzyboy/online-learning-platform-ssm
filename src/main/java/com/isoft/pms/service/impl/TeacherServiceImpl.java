package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Teacher;
import com.isoft.pms.dao.ITeacherDao;
import com.isoft.pms.exception.TeacherServiceException;
import com.isoft.pms.service.ITeacherService;
import com.isoft.pms.unit.StringUnit;
import com.isoft.pms.unit.TeacherCode;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author gaixiankang
 * service实现类
 */
@Service
public class TeacherServiceImpl implements ITeacherService {

    @Resource
    private ITeacherDao dao;

    @Override
    public boolean addTeacher(Teacher teacher) {
        Teacher teacher1 = dao.selectByUserId(teacher.getUserId());
        if (StringUnit.isNull(teacher.getTeacherName())) {
            throw new TeacherServiceException(TeacherCode.TEACHER_NAME_ISNULL);
        }else if(StringUnit.isNull(teacher.getTeacherId())) {
            throw new TeacherServiceException(TeacherCode.TEACHER_ID_ISNULL);
        }else if(StringUnit.isNull(teacher.getTeacherSex())) {
            throw new TeacherServiceException(TeacherCode.TEACHER_SEX_ISNULL);
        }else if(StringUnit.isNull(teacher.getStatus())) {
            throw new TeacherServiceException(TeacherCode.TEACHER_STATUS_ISNULL);
        }else if(StringUnit.isNull(teacher.getUserId())) {
            throw new TeacherServiceException(TeacherCode.TEACHER_USER_ID_ISNULL);
        }else if(teacher.getTeacherName().length() > 50) {
            throw new TeacherServiceException(TeacherCode.TEACHER_NAME_SIZE_OUT50);
        }else if(teacher.getStatus() != 0 && teacher.getStatus() != 1 && teacher.getStatus() != 2) {
            throw new TeacherServiceException(TeacherCode.TEACHER_STATUS_NOT_EXIST);
        }else if(teacher.getTeacherSex() != 0 && teacher.getTeacherSex() != 1) {
            throw new TeacherServiceException(TeacherCode.TEACHER_SEX_NOT_EXIST);
        }else if(teacher1 != null) {
            throw new TeacherServiceException(TeacherCode.TEACHER_USER_ID_ALREADY_EXIST);
        }
        return dao.addTeacher(teacher);

    }

    @Override
    public boolean deleteTeacher(Long id) {
        return dao.deleteById(id);
    }

    @Override
    public boolean updateTeacher(Teacher teacher) {
        return dao.updateById(teacher);
    }

    @Override
    public List<Teacher> selectAllTeacher() {
        return dao.selectTeachers(new Teacher());
    }

    @Override
    public List<Teacher> selectTeacher(Teacher teacher) {
        return dao.selectTeachers(teacher);
    }

    @Override
    public boolean fakeDeleteTeacher(Long id) {
        return dao.fakeDeleteTeacher(id);
    }


}
