package com.isoft.pms.controller;

import com.isoft.pms.bean.Teacher;
import com.isoft.pms.service.ITeacherService;
import com.isoft.pms.unit.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @author gaixiankang
 */
@Controller
@RequestMapping("/teacher")
@CrossOrigin(maxAge = 3600)
@Api(description = "教师管理测试接口")
public class TeacherController {

    @Autowired
    private ITeacherService service;

    @ApiOperation(value = "添加教师测试")
    @ResponseBody
    @PutMapping("/teacher-add")
    public AjaxResult addTeacher(@RequestBody Teacher teacher){
        return AjaxResult.success("添加成功",service.addTeacher(teacher));
    }

    @ApiOperation(value = "删除教师测试")
    @ResponseBody
    @DeleteMapping("/teacher-deleteById")
    public AjaxResult delete(Long id){
        return AjaxResult.success("删除成功",service.deleteTeacher(id));
    }

    @ApiOperation(value = "修改教师测试")
    @ResponseBody
    @PostMapping("/teacher-updateById")
    public AjaxResult update(@RequestBody Teacher teacher){
        return AjaxResult.success("更改成功", service.updateTeacher(teacher));
    }

    @ApiOperation(value= "查询所有教师信息")
    @ResponseBody
    @GetMapping("/teacher-selectAllTeacher")
    public AjaxResult selectAllTeacher() {
        return AjaxResult.success("查询成功",service.selectAllTeacher());
    }

    @ApiOperation(value= "模糊查询教师信息")
    @ResponseBody
    @GetMapping("/teacher-selectTeacher")
    public AjaxResult selectTeacher(@RequestBody Teacher teacher) {
        return AjaxResult.success("查询成功",service.selectTeacher(teacher));
    }

    @ApiOperation(value= "逻辑删除教师信息")
    @ResponseBody
    @DeleteMapping("/teacher-fakeDeleteById")
    public AjaxResult fakeDeleteTeacher(Long id) {
        return AjaxResult.success("逻辑删除成功",service.fakeDeleteTeacher(id));
    }
}
