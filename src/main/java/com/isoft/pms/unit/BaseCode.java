package com.isoft.pms.unit;

/**
 * @author zhouzhenyuan
 */
public interface BaseCode {
    /**
     * 基础业务码实现接口
     * @return
     */
    int value();
}
