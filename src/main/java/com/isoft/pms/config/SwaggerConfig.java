package com.isoft.pms.config;


import io.swagger.annotations.Api;
import io.swagger.annotations.AuthorizationScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouzhenyuan
 * swagger-ui 配置类
 */
@Configuration
@EnableWebMvc
@EnableSwagger2
@ComponentScan("com.isoft.pms")
public class SwaggerConfig extends WebMvcConfigurationSupport {
    /**
     * 初始化springmvc容器 搭建swagger环境
     * @return
     */
    @Bean
    public Docket createRestApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(securitySchemes())
                .groupName("开发环境");
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("在线学习平台的接口文档")
                .description("在线学习平台的接口测试")
                .version("0.0.1")
                .license("")
                .licenseUrl("")
                .build();
    }

    private List<ApiKey> securitySchemes() {
        return newArrayList(
                new ApiKey("Authorization", "Authorization", "header"));
    }
}
