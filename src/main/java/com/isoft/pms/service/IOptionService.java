package com.isoft.pms.service;

import com.isoft.pms.bean.Option;
import java.util.List;

/**
 * @author shangzhiwei
 */

public interface IOptionService {

    /**
     * 添加选项接口
     * @param option
     * @return
     */
    boolean addOption(Option option);

    /**
     * 查询所有选项接口
     * @return
     */
    List<Option> selectAllOption();

    /**
     * 根据id删除选项接口
     * @param id
     * @return
     */
    boolean deleteOption(Long id);

    /**
     * 根据id修改选项接口
     * @param option
     * @return
     */
    boolean updateOption(Option option);

    /**
     * 根据id逻辑删除选项接口
     * @param id
     * @return
     */
    boolean fakeDeleteOption(Long id);

    /**
     * 模糊查询选项接口
     * @param option
     * @return
     */
    List<Option> selectOption(Option option);
}
