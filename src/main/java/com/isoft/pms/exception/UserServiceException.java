package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.UserCode;

/**
 * 用户业务异常类
 *
 * @author huxiongwei
 */

public class UserServiceException extends BaseServiceException {

    public UserServiceException(UserCode code) {
        super(code);
        putIt(UserCode.USER_NAME_ISNULL, "用户名为空").
                putIt(UserCode.USER_NAME_ERROR, "用户名不存在").
                putIt(UserCode.USER_NAME_EXIST, "用户名已存在").
                putIt(UserCode.USER_PASSWORD_ISNULL, "密码为空").
                putIt(UserCode.USER_PASSWORD_ERROR, "密码错误").
                putIt(UserCode.USER_PHOTO_SIZE_OUT10M, "头像文件超过10M").
                putIt(UserCode.USER_CREATER_ISNULL, "创建者为空").
                putIt(UserCode.USER_CREATER_ERROR, "创建者错误").
                putIt(UserCode.USER_STATUS_NOT_EXIST, "状态码不存在").
                putIt(UserCode.USER_STATUS_ISNULL, "状态码为空").
                putIt(UserCode.USER_ROLE_ID_ISNULL, "角色编号为空").
                putIt(UserCode.USER_ROLE_ID_ERROR, "角色编号错误").
                putIt(UserCode.USER_DEPT_ID_ISNULL, "部门编号为空").
                putIt(UserCode.USER_DEPT_ID_ERROR, "部门编号错误").
                putIt(UserCode.USER_SALT_ISNULL, "盐度值为空").
                putIt(UserCode.USER_CREATETIME_ISNULL, "创建时间为空")
                .putIt(UserCode.USER_NOT_LOGIN, "用户未登录")
                .putIt(UserCode.USER_ID_ISNULL,"用户编号为空")
        ;
    }

}
