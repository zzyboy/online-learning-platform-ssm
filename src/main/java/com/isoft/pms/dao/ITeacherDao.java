package com.isoft.pms.dao;

import com.isoft.pms.bean.Teacher;
import com.isoft.pms.dao.sql.TeacherSQL;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author gaixiankang
 */
public interface ITeacherDao {

    /**
     * 添加教师
     * @param teacher
     * @return
     */
    @Insert("insert into teacher(teacher_id,teacher_name,teacher_sex,status,user_id) values(#{teacherId},#{teacherName},#{teacherSex},#{status},#{userId})")
    boolean addTeacher(Teacher teacher);

    /**
     * 添加教师时，查看用户id是否重复
     * @param userId
     * @return
     */
    @Select("select * from teacher where user_id=#{userId}")
    Teacher selectByUserId(Long userId);

    /**
     *根据id删除教师信息
     * @param id
     * @return
     */
    @Delete("delete from teacher where id=#{id}")
    boolean deleteById(@Param("id") Long id);

    /**
     *根据id更新教师信息
     * @param teacher
     * @return
     */
    @UpdateProvider(type = TeacherSQL.class,method = "createUpdateTeacherSQL")
    boolean updateById(Teacher teacher);

    /**
     *查看教师信息
     * @param teacher
     * @return
     */
    List<Teacher> selectTeachers(Teacher teacher);

    /**
     * 根据id逻辑删除教师
     * @param id
     * @return
     */
    @Update("update teacher set status=1 where id=#{id}")
    boolean fakeDeleteTeacher(Long id);



}
