package com.isoft.pms.dao;

import com.isoft.pms.bean.Job;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author shangzhiwei
 */

@Mapper
public interface IJobDao {

    /**
     * 添加任务
     * @param job
     * @return
     */
    boolean addJob(Job job);

    /**
     * 查询所有任务
     * @param job
     * @return
     */
    List<Job> selectJobs(Job job);

    /**
     * 根据id删除任务
     * @param id
     * @return
     */
    boolean deleteJob(Long id);

    /**
     * 根据id修改任务
     * @param job
     * @return
     */
    boolean updateJob(Job job);

    /**
     * 根据id逻辑删除任务
     * @param id
     * @return
     */
    @Update("update job set status=1 where id=#{id}")
    boolean fakeDeleteJob(Long id);
}
