/*
 Navicat Premium Data Transfer

 Source Server         : mac
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : learning_platform

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 27/12/2021 01:06:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `parent_id` bigint DEFAULT NULL COMMENT '父部门',
  `leader` bigint DEFAULT NULL COMMENT '领导',
  `status` varchar(255) DEFAULT NULL COMMENT '0正常1删除2停用',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for job
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `id` bigint NOT NULL COMMENT '任务编号',
  `create_id` bigint DEFAULT NULL COMMENT '创建者编号',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型0:为考试，1:为作业',
  `greate_time` datetime DEFAULT NULL COMMENT '创建时间',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int DEFAULT NULL COMMENT '0正常1删除2停用',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '总分',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for job_question
-- ----------------------------
DROP TABLE IF EXISTS `job_question`;
CREATE TABLE `job_question` (
  `id` bigint NOT NULL COMMENT '任务题库编号',
  `job_id` bigint DEFAULT NULL COMMENT '任务编号',
  `question_id` bigint DEFAULT NULL COMMENT '题目编号',
  `status` int DEFAULT NULL COMMENT '0正常1删除2停用',
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  KEY `job_id` (`job_id`),
  CONSTRAINT `job_id` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job_question
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for option
-- ----------------------------
DROP TABLE IF EXISTS `option`;
CREATE TABLE `option` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '选项编号',
  `option_content` varchar(200) NOT NULL COMMENT '选项内容',
  `status` int NOT NULL DEFAULT '0' COMMENT '0正常1删除2停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of option
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `permission_name` varchar(200) NOT NULL COMMENT '权限名称',
  `url` varchar(200) NOT NULL COMMENT '权限接口',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `status` int NOT NULL DEFAULT '0' COMMENT '0正常1删除2停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` bigint NOT NULL COMMENT '题目编号',
  `user_id` bigint DEFAULT NULL COMMENT '上传者编号',
  `question_content` text NOT NULL COMMENT '题目题干',
  `question_type` int NOT NULL COMMENT '0单选题1多选题2判断题3纯文字题4带图带文字题',
  `answer` text NOT NULL COMMENT '答案',
  `score` varchar(255) NOT NULL COMMENT '分数',
  `level` varchar(255) DEFAULT NULL COMMENT '难易程度',
  `major_id` bigint DEFAULT NULL,
  `status` int DEFAULT '0' COMMENT '0正常1删除2停用',
  `isobjective` int DEFAULT NULL COMMENT '是否为客观题0不是，1是',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for question_option
-- ----------------------------
DROP TABLE IF EXISTS `question_option`;
CREATE TABLE `question_option` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '选项题目编号',
  `option_id` bigint NOT NULL COMMENT '选项编号',
  `question_id` bigint NOT NULL COMMENT '问题编号',
  `status` int NOT NULL DEFAULT '0' COMMENT '0正常1删除2停用',
  PRIMARY KEY (`id`),
  KEY `option_id` (`option_id`),
  KEY `question_id2` (`question_id`),
  CONSTRAINT `option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `question_id2` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question_option
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `role_name` varchar(200) NOT NULL COMMENT '角色的名称',
  `description` varchar(200) DEFAULT NULL COMMENT '角色的描述信息',
  `status` varchar(255) NOT NULL DEFAULT '0' COMMENT '状态：0正常1删除2停用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `permission_id` bigint DEFAULT NULL COMMENT '权限编号',
  `role_id` bigint DEFAULT NULL COMMENT '角色编号',
  `status` varchar(255) NOT NULL DEFAULT '0' COMMENT '0正常1删除2停用',
  PRIMARY KEY (`id`),
  KEY `permission` (`permission_id`),
  KEY `role_id2` (`role_id`),
  CONSTRAINT `permission` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`),
  CONSTRAINT `role_id2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '学生编号',
  `user_id` bigint DEFAULT NULL COMMENT '用户编号',
  `student_id` bigint DEFAULT NULL COMMENT '学号',
  `student_name` varchar(200) DEFAULT NULL COMMENT '学生姓名',
  `student_sex` varchar(200) DEFAULT NULL COMMENT '学生性别',
  `major_id` bigint DEFAULT NULL COMMENT '所属专业',
  `status` int DEFAULT NULL COMMENT '0正常1删除2停用',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` bigint NOT NULL COMMENT '教师编号',
  `teacher_id` bigint DEFAULT NULL COMMENT '教师号',
  `teacher_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '教师姓名',
  `teacher_sex` int DEFAULT NULL COMMENT '0女1男',
  `status` varchar(255) DEFAULT NULL COMMENT '0正常1删除2停用',
  `user_id` bigint NOT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`),
  KEY `user_id2` (`user_id`),
  CONSTRAINT `user_id2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `username` varchar(200) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '用户密码',
  `salt` varchar(200) NOT NULL COMMENT '盐度',
  `photo` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `creater` varchar(200) NOT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` varchar(200) NOT NULL DEFAULT '0' COMMENT '状态0正常1逻辑删除2停用',
  `role_id` bigint NOT NULL COMMENT 'role表中对应的角色',
  `dept_id` bigint NOT NULL COMMENT '部门id',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `dept_id` (`dept_id`),
  CONSTRAINT `dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
