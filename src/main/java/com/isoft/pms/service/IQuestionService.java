package com.isoft.pms.service;

import com.isoft.pms.bean.Question;

import java.util.List;
import java.util.Map;

/**
 * @author shangzhiwei
 */

public interface IQuestionService {

    /**
     * 添加问题接口
     * @param question
     * @return
     */
    boolean addQuestion(Question question);

    /**
     * 根据id删除问题接口
     * @param id
     * @return
     */
    Map<String,Object> deleteByQuestionId(int id);

    /**
     * 查询所有问题接口
     * @return
     */
    List<Question> selectAllQuestion();

    /**
     * 根据id修改问题接口
     * @param question
     * @return
     */
    Map<String,Object> updateQuestion(Question question);

    /**
     * 根据id逻辑删除问题接口
     * @param id
     * @return
     */
    boolean fakeDeleteQuestion(Long id);

    /**
     * 模糊查询问题接口
     * @param question
     * @return
     */
    List<Question> selectQuestion(Question question);
}
