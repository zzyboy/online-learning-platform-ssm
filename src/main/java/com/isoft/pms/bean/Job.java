package com.isoft.pms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * @author shangzhiwei
 * 任务实体类
 */

@ApiModel(description = "任务信息")
@Data
public class Job {

    @ApiModelProperty(value = "任务编号")
    private Long id;
    @ApiModelProperty(value = "创建者编号")
    private Long createId;
    @ApiModelProperty(value = "任务类型")
    private Integer type;
    @ApiModelProperty(value = "任务创建时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty(value = "任务开始时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @ApiModelProperty(value = "任务结束时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    @ApiModelProperty(value = "任务状态")
    private Integer status;
    @ApiModelProperty(value = "部门编号")
    private Long deptId;
    @ApiModelProperty(value = "任务备注")
    private String remarks;
    @ApiModelProperty(value = "任务总分")
    private Integer score;
}
