const mutations = {
    isCollapse(state) {
        state.isCollapse = !state.isCollapse;
    },
    editBread(state,name) {
        state.bread = name;
    },
    addTag(state,tagName) {
        state.tags.set(tagName.name,tagName.path)
    },
    removeTag(state,tagName) {
        state.tags.delete(tagName)
    },
    setThemeColor(state, curcolor){
        state.themecolor = curcolor
    }
}

export default mutations