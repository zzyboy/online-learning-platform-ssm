package com.isoft.pms.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

/**
 * @author shangzhiwei
 * 选项实体类
 */

@ApiModel(value = "选项信息")
@Data
public class Option {

    @ApiModelProperty(value = "选项编号")
    private Long id;
    @ApiModelProperty(value = "选项内容")
    private String optionContent;
    @ApiModelProperty(value = "选项状态")
    private Integer status;
}
