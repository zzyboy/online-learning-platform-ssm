package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Student;
import com.isoft.pms.dao.IStudentDao;
import com.isoft.pms.exception.StudentServiceException;
import com.isoft.pms.service.IStudentService;
import com.isoft.pms.unit.StringUnit;
import com.isoft.pms.unit.StudentCode;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author gaixiankang
 */
@Service
public class StudentServiceImpl implements IStudentService {

    @Resource
    private IStudentDao dao;

    @Override
    public boolean addStudent(Student student) {
        Student student1 = dao.selectByUserId(student.getUserId());
        if(StringUnit.isNull(student.getStudentName())) {
            throw new StudentServiceException(StudentCode.STUDENT_NAME_ISNULL);
        }else if(student1 != null){
            throw new StudentServiceException(StudentCode.STUDENT_USER_ID_ALREADY_EXIST);
        }else if(StringUnit.isNull(student.getUserId())) {
            throw new StudentServiceException(StudentCode.STUDENT_USER_ID_ISNULL);
        }else if(StringUnit.isNull(student.getStudentId())) {
            throw new StudentServiceException(StudentCode.STUDENT_ID_ISNULL);
        }else if(StringUnit.isNull(student.getStudentSex())) {
            throw new StudentServiceException(StudentCode.STUDENT_SEX_ISNULL);
        }else if(StringUnit.isNull(student.getMajorId())) {
            throw new StudentServiceException(StudentCode.STUDENT_MAJOR_ID_ISNULL);
        }else if(StringUnit.isNull(student.getStatus())) {
            throw new StudentServiceException(StudentCode.STUDENT_STATUS_ISNULL);
        }else if(student.getStudentName().length() > 50) {
            throw new StudentServiceException(StudentCode.STUDENT_NAME_SIZE_OUT50);
        }else if(student.getStatus() != 0 && student.getStatus() != 1 && student.getStatus() != 2) {
            throw new StudentServiceException(StudentCode.STUDENT_STATUS_NOT_EXIST);
        }else if(student.getStudentSex() != 0 && student.getStudentSex() != 1) {
            throw new StudentServiceException(StudentCode.STUDENT_SEX_NOT_EXIST);
        }
        return dao.addStudent(student);
    }

    @Override
    public boolean deleteStudent(Long id) {
        return dao.deleteById(id);
    }

    @Override
    public boolean updateStudent(Student student) {
        return dao.updateById(student);
    }

    @Override
    public List<Student> selectAllStudent() {
        return dao.selectStudents(new Student());
    }

    @Override
    public List<Student> selectStudent(Student student) {
        return dao.selectStudents(student);
    }

    @Override
    public boolean fakeDeleteStudent(Long id) {
        return dao.fakeDeleteStudent(id);
    }
}
