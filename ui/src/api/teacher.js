import request from '@/unit/request'

export function addTeacher(teacher) {
    return request({
        url: '/teacher/teacher-add',
        method: 'put',
        data: teacher
    });
}

export function deleteTeacher(id) {
    return request({
        url: '/teacher/teacher-deleteById',
        method: 'delete',
        data: id
    });
}

export function updateTeacher(teacher) {
    return request({
       url: '/teacher/teacher-updateById',
       method: 'post',
        data: teacher
    });
}

export function selectAllTeacher() {
    return request({
        url: '/teacher/teacher-selectAllTeacher',
        method: 'get'
    });
}

export function selectTeacher(teacher) {
    return request({
        url: '/teacher/teacher-selectTeacher',
        method: 'get',
        data: teacher
    });
}

export function fakeDeleteTeacher(id) {
    return request({
        url: `/teacher/teacher-fakeDeleteById?id=${id}`,
        method: 'delete',
    });
}

