package com.isoft.pms.bean;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhouzhenyuan
 */
@Data
@ApiModel(value = "部门信息")
public class Dept {

    @ApiModelProperty(value = "部门编号")
    private Long id;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "父部门编号")
    private Long parentId;

    @ApiModelProperty(value = "领导用户编号")
    private Integer leader;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remarks;
}
