package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.QuestionCode;

/**
 * 问题业务异常类
 * @author shangzhiwei
 */
public class QuestionServiceException extends BaseServiceException {

    public QuestionServiceException(QuestionCode code){
        super(code);
        putIt(QuestionCode.QUESTION_CONTENT_ISNULL,"题目题干为空").
                putIt(QuestionCode.QUESTION_TYPE_ISNULL,"题目类型为空").
                putIt(QuestionCode.QUESTION_ANSWER_ISNULL,"题目答案为空").
                putIt(QuestionCode.QUESTION_SCORE_ISNULL,"题目分数为空").
                putIt(QuestionCode.QUESTION_USER_ID_NOT_EXIST,"上传者编号不存在").
                putIt(QuestionCode.QUESTION_TYPE_NOT_EXIST,"题目类型不存在").
                putIt(QuestionCode.QUESTION_SCORE_LESSZERO,"题目分数小于等于零").
                putIt(QuestionCode.QUESTION_MAJOR_ID_NOT_EXIST,"部门编号不存在").
                putIt(QuestionCode.QUESTION_STATUS_ISNULL,"状态码为空").
                putIt(QuestionCode.QUESTION_STATUS_NOT_EXIST,"状态码不存在").
                putIt(QuestionCode.QUESTION_ISOBJECTIVE_NOT_EXIST,"判断是否为客观题码不存在").
                putIt(QuestionCode.QUESTION_REMARK_SIZE_OUT200,"备注超过200字").
                putIt(QuestionCode.QUESTION_CONTENT_EXIST,"题目已经存在");
    }
}
