package com.isoft.pms.unit;

import org.springframework.util.DigestUtils;

import java.util.Random;

/**
 * @author huxiongwei
 */
public class PassWordUtil {
    //产生盐
    public static final String createSalt(){
        Random ran=new Random();
        //随机在基础字符里产生盐
        String base="0123456789yasgdashuidhuiashdasuidhsad";
        StringBuffer str=new StringBuffer(16);
        for(int i=0;i<16;i++){
            str.append(base.charAt(ran.nextInt(base.length())));
        }
        return str.toString();
    }

    //进行加密
    public static final String encryption(String pass,String salt,int count){
        for(int i=0;i<count;i++){
            pass+=salt;
        }
        //进行MD5加密
        pass= DigestUtils.md5DigestAsHex(pass.getBytes());
        return pass;
    }

    /**
     *
     * @param pass
     * @param dbpass
     * @param salt
     * @param count
     * @return
     */
    public static final boolean isRealPass(String pass,String dbpass,String salt,int count){
        return dbpass.equals(encryption(pass,salt,count));
    }

}
