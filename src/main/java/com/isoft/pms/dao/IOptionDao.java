package com.isoft.pms.dao;

import com.isoft.pms.bean.Option;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author shangzhiwei
 */

@Mapper
public interface IOptionDao {

    /**
     * 添加选项
     * @param option
     * @return
     */
    boolean addOption(Option option);

    /**
     * 查询所有选项
     * @param option
     * @return
     */
    List<Option> selectOptions(Option option);

    /**
     * 根据id删除选项
     * @param id
     * @return
     */
    boolean deleteOption(Long id);

    /**
     * 根据id修改选项
     * @param option
     * @return
     */
    boolean updateOption(Option option);

    /**
     * 根据id逻辑删除选项
     * @param id
     * @return
     */
    @Update("update `option` set status=1 where id=#{id}")
    boolean fakeDeleteOption(Long id);
}
