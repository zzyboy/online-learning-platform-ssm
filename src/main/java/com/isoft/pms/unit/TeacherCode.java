package com.isoft.pms.unit;

/**
 * 状态码枚举类
 * @author gaixiankang
 */
public enum TeacherCode implements BaseCode{

    /**
     * 状态码
     */
    TEACHER_ID_ISNULL(4001),
    TEACHER_NAME_ISNULL(4002),
    TEACHER_SEX_ISNULL(4003),
    TEACHER_STATUS_ISNULL(4004),
    TEACHER_USER_ID_ISNULL(4005),
    TEACHER_ID_ALREADY_EXIST(4006),
    TEACHER_NAME_SIZE_OUT50(4007),
    TEACHER_STATUS_NOT_EXIST(4008),
    TEACHER_USER_ID_NOT_EXIST(4009),
    TEACHER_SEX_NOT_EXIST(4010),
    TEACHER_USER_ID_ALREADY_EXIST(4011),
    ;
    private final int i;
    TeacherCode(int i) {
        this.i = i;
    }
    @Override
    public int value() {
        return this.i;
    }

}
