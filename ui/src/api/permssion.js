import request from "@/unit/request";

export function getAllPermission() {
    return request({
        url:'permission/permission-getAll',
        method:'get'
    })
}

export function addPermission(data) {
    return request({
        url:'permission/permission-add',
        method:'put',
        data
    })
}