package com.isoft.pms.bean;

import lombok.Data;

/**
 * @author zhouzhenyuan
 */
@Data
public class OnlineUser {
    /**
     * 会话编号
     */
    private String token;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户角色名
     */
    private String roleName;
    /**
     *  用户公网ip
     */
    private String ip;
    /**
     * 用户地址
     */
    private String address;
    /**
     * 用户浏览器内核
     */
    private String browser;
    /**
     * 用户系统
     */
    private String system;
    /**
     * 用户登录时间
     */
    private String loginTime;
}
