package com.isoft.pms.service.impl;

import com.isoft.pms.bean.Question;
import com.isoft.pms.dao.IQuestionDao;
import com.isoft.pms.exception.QuestionServiceException;
import com.isoft.pms.service.IQuestionService;
import com.isoft.pms.unit.QuestionCode;
import com.isoft.pms.unit.StringUnit;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shangzhiwei
 */

@Service
public class QuestionServiceImpl implements IQuestionService {

    @Resource
    private IQuestionDao dao;

    @Override
    public boolean addQuestion(Question question) {
        Question question1 = dao.selectByQuestionContent(question.getQuestionContent());
        if(question1 != null) {
            throw new QuestionServiceException(QuestionCode.QUESTION_CONTENT_EXIST);
        }else if(StringUnit.isNull(question.getQuestionContent())){
            throw new QuestionServiceException(QuestionCode.QUESTION_CONTENT_ISNULL);
        }else if(StringUnit.isNull(question.getQuestionType())){
            throw new QuestionServiceException(QuestionCode.QUESTION_TYPE_ISNULL);
        }else if(StringUnit.isNull(question.getAnswer())){
            throw new QuestionServiceException(QuestionCode.QUESTION_ANSWER_ISNULL);
        }else if(StringUnit.isNull(question.getScore())){
            throw new QuestionServiceException(QuestionCode.QUESTION_SCORE_ISNULL);
        }else if(StringUnit.isNull(question.getUserId())){
            throw new QuestionServiceException(QuestionCode.QUESTION_USER_ID_NOT_EXIST);
        }else if(question.getQuestionType() != 0 && question.getQuestionType() != 1 && question.getQuestionType() != 2 && question.getQuestionType() != 3 && question.getQuestionType() != 4){
            throw new QuestionServiceException(QuestionCode.QUESTION_TYPE_NOT_EXIST);
        }else if(question.getScore() <= 0){
            throw new QuestionServiceException(QuestionCode.QUESTION_SCORE_LESSZERO);
        }else if(StringUnit.isNull(question.getMajorId())){
            throw new QuestionServiceException(QuestionCode.QUESTION_MAJOR_ID_NOT_EXIST);
        }else if(StringUnit.isNull(question.getStatus())){
            throw new QuestionServiceException(QuestionCode.QUESTION_STATUS_ISNULL);
        }else if(question.getStatus() != 0 && question.getStatus() != 1 && question.getStatus() !=2){
            throw new QuestionServiceException(QuestionCode.QUESTION_STATUS_NOT_EXIST);
        }else if(question.getIsObjective() != 0 && question.getIsObjective() != 1){
            throw new QuestionServiceException(QuestionCode.QUESTION_ISOBJECTIVE_NOT_EXIST);
        }else if(question.getRemark().length() > 200){
            throw new QuestionServiceException(QuestionCode.QUESTION_REMARK_SIZE_OUT200);
        }
        return dao.addQuestion(question);
    }

    @Override
    public Map<String, Object> deleteByQuestionId(int id) {

        Map<String,Object> map = new HashMap<>();
        int temp = dao.deleteByQuestionId(id);
        if(temp>0){
            map.put("msg","删除成功");
            map.put("code","200");
        }else{
            map.put("msg","删除失败");
            map.put("code","600");
        }
        return map;
    }

    @Override
    public List<Question> selectAllQuestion() { return dao.selectQuestions(new Question()); }

    @Override
    public Map<String, Object> updateQuestion(Question question) {

        Map<String,Object> map = new HashMap<>();
        int temp = dao.updateQuestion(question);
        if(temp>0){
            map.put("msg","修改成功");
            map.put("code","200");
        }else{
            map.put("msg","修改失败");
            map.put("code","600");
        }
        return map;
    }

    @Override
    public boolean fakeDeleteQuestion(Long id) {
        return dao.fakeDeleteQuestion(id);
    }

    @Override
    public List<Question> selectQuestion(Question question) {
        return dao.selectQuestions(question);
    }
}
