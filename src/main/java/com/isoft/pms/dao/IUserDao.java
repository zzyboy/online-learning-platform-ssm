package com.isoft.pms.dao;


import com.isoft.pms.bean.User;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;


import java.util.List;

/**
 * @author huxiongwei
 */
@Mapper
public interface IUserDao {

    boolean addUser(User user);

    /*
    * 根据用户名查找用户的全部信息
    * */

    @Select("select * from user where username=#{username}")
    User selectByName(String username);


    boolean deleteUser(Long id);


    List<User> getUser(User user);

    boolean updateUser(User user);


}
