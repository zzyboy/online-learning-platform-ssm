import request from '@/unit/request'

export function addStudent(student) {
    return request({
        url: '/student/student-add',
        method: 'put',
        data: student
    });
}

export function deleteStudent(id) {
    return request({
        url:'/student/student-deleteById',
        method:'delete',
        data: id
    });
}

export function updateStudent(student) {
    return request({
        url:'/student/student-updateById',
        method:'post',
        data: student
    });
}

export function selectAllStudent() {
    return request({
        url: '/student/student-selectAllStudent',
        method: 'get'
    });
}

export function selectStudent(student) {
    return request({
        url: '/student/student-selectStudent',
        method: 'get',
        data: student
    });
}

export function fakeDeleteStudent(id) {
    return request({
        url: '/student/student-fakeDeleteById',
        method: 'delete',
        date: id
    });
}