package com.isoft.pms.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author gaixiankang
 * 老师实体类
 */
@ApiModel(value = "教师信息")
@Data
public class Teacher {

    @ApiModelProperty(value = "教师编号")
    private Long id;
    @ApiModelProperty(value = "教师号")
    private Long teacherId;
    @ApiModelProperty(value = "教师姓名")
    private String teacherName;
    @ApiModelProperty(value = "教师性别")
    private Integer teacherSex;
    @ApiModelProperty(value = "状态")
    private Integer status;
    @ApiModelProperty(value = "用户编号")
    private Long userId;

}
