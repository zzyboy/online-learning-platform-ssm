package com.isoft.pms.unit;

/**
 * 状态码枚举类
 * @author shangzhiwei
 */
public enum JobCode implements BaseCode{
    /**
     * 状态码
     */
    JOB_CREATE_ID_ISNULL(9001),
    JOB_CREATE_ID_NOT_EXIST(9002),
    JOB_TYPE_ISNULL(9003),
    JOB_TYPE_NOT_EXIST(9004),
    JOB_START_TIME_ISNULL(9005),
    JOB_START_TIME_LESS_CREATE_TIME(9006),
    JOB_END_TIME_ISNULL(9007),
    JOB_END_TIME_LESS_START_TIME(9008),
    JOB_STATUS_ISNULL(9009),
    JOB_STATUS_NOT_EXIST(9010),
    JOB_DEPT_ID_NOT_EXIST(9011),
    JOB_REMARK_OUT250(9012),
    JOB_SCORE_LESSZERO(9013),
    ;
    private final int i;
    JobCode(int i) {this.i = i; }
    @Override
    public int value() {return this.i;}
}
