package com.isoft.pms.dao;

import com.isoft.pms.bean.Role;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author zhouzhenyuan
 */
@Mapper
public interface IRoleDao {
    /**
     *  添加角色
     * @param role 角色实体类
     * @return
     */
    boolean addRole(Role role);

    /**
     * 通过条件获得角色
     * 当status为空的时候默认取0正常
     * @param role
     * @return
     */
    List<Role> getRoles(Role role);


    /** 根据id删除角色id
     * 删除角色
     * @param id
     * @return
     */
    boolean deleteRole(Long id);

    /**
     * 根据id更新角色信息
     * @param role 必须带有主键id
     * @return
     */
    boolean updateRole(Role role);

    /**
     *  精准获得角色
     * @param role
     * @return
     */
    List<Role> getRole(Role role);
}
