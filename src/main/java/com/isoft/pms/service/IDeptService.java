package com.isoft.pms.service;

import com.isoft.pms.bean.Dept;

import java.util.List;

/**
 * @author zhouzhenyuan
 */
public interface IDeptService {
    /**
     * 获得全部部门信息
     * @return 返回部门列表
     */
    List<Dept> getAllDept();

    /**
     * 添加部门信息
     * @param dept 部门实体信息
     * @return 操作成功返回true 操作失败返回false
     */
    boolean addDept(Dept dept);

    /**
     * 根据id 逻辑删除部门
     * @param id 部门编号
     * @return 操作成功返回true 操作失败返回false
     */
    boolean fakeDeleteDeptById(Long id);

    /**
     *  根据id 物理删除部门
     * @param id 部门编号
     * @return 操作成功返回true 操作失败返回false
     */
    boolean deleteDeptById(Long id);

    /**
     * 根据id更新部门
     * @param dept 部门实体
     * @return 操作成功返回true 操作失败返回false
     */
    boolean updateDept(Dept dept);

    /**
     * 根据条件模糊搜索部门
     * @param dept 部门实体类
     * @return 部门信息列表
     */
    List<Dept> getDept(Dept dept);

    /**
     * 判断部门id是否在数据库中
     *
     * @param id 部门主键
     * @return true在 false不在
     */
    public boolean deptIdExit(Long id);
}
