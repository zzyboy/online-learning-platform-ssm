import request from "@/unit/request";


export async function addOption(option) {
    return request({
        url: '/option/option-add',
        method: 'put',
        data:option
    });
}

export async function selectAllOption() {
    return request({
        url: '/option/option-selectAllOption',
        method: 'get'
    });
}

export async function deleteOption(id) {
    return request({
        url: '/option/option-delete',
        method: 'delete',
        data: id
    });
}

export async function updateOption(option) {
    return request({
        url: '/option/option-update',
        method: 'post',
        data:option
    });
}

export async function fakeDeleteOption(id) {
    return request({
        url: `/option/option-fakeDeleteById?id=${id}`,
        method: 'delete',
    });
}

export async function selectOption(option) {
    return request({
        url: '/option/option-selectOption',
        method: 'get',
        data:option
    });
}