package com.isoft.pms.dao.sql;

import com.isoft.pms.bean.Teacher;
import org.apache.ibatis.jdbc.SQL;

/**
 * @author gaixiankang
 * SQL动态构建器
 */
public class TeacherSQL {

    public String createUpdateTeacherSQL(final Teacher teacher){
        return new SQL(){{
            UPDATE("teacher");
            if (teacher.getTeacherId()!=0){
                SET("teacher_id="+teacher.getTeacherId());
            }
            if(teacher.getTeacherName()!=null){
                SET("teacher_name=#{teacherName}");
            }
            if(teacher.getTeacherSex()!=0){
                SET("teacher_sex="+teacher.getTeacherSex());
            }
            if(teacher.getStatus()!=0){
                SET("status="+teacher.getStatus());
            }
            if(teacher.getUserId()!=0){
                SET("user_id="+teacher.getUserId());
            }
            WHERE("id="+teacher.getId());
        }}.toString();
    }

}
