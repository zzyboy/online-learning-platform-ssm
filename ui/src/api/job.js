import request from "@/unit/request";


export async function addJob(job) {
    return request({
        url: '/job/job-add',
        method: 'put',
        data:job
    });
}

export async function selectAllJob() {
    return request({
        url: '/job/job-selectAllJob',
        method: 'get'
    });
}

export async function deleteJob(id) {
    return request({
        url: '/job/job-delete',
        method: 'delete',
        data:id
    });
}

export async function updateJob(job) {
    return request({
        url: '/job/job-update',
        method: 'post',
        data:job
    });
}

export async function fakeDeleteJob(id) {
    return request({
        url: `/job/job-fakeDeleteById?id=${id}`,
        method: 'delete',
    });
}

export async function selectJob(job) {
    return request({
        url: '/job/job-selectJob',
        method: 'get',
        data:job
    });
}