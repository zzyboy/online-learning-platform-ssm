package com.isoft.pms.service;

import com.isoft.pms.bean.Student;

import java.util.List;
import java.util.Map;

/**
 * @author gaixiankang
 */
public interface IStudentService {

    /**
     * 添加学生接口
     * @param student
     * @return
     */
    boolean addStudent(Student student);

    /**
     * 根据id删除学生接口
     * @param id
     * @return
     */
    boolean deleteStudent(Long id);


    /**
     * 动态更新学生接口
     * @param student
     * @return
     */
    boolean updateStudent(Student student);

    /**
     * 查询所有学生接口
     * @return
     */
    List<Student> selectAllStudent();

    /**
     * 模糊查询接口
     * @param student
     * @return
     */
    List<Student> selectStudent(Student student);

    /**
     * 根据id逻辑删除学生
     * @param id
     * @return 操作成功返回true 操作失败返回 false
     */
    boolean fakeDeleteStudent(Long id);
}
