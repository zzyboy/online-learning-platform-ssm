package com.isoft.pms.exception.baseException;

import com.isoft.pms.unit.BaseCode;
import com.isoft.pms.unit.DeptCode;
import com.isoft.pms.unit.TeacherCode;
import lombok.Getter;

import java.util.HashMap;

/** 业务异常基础类
 *
 * @author zhouzhenyuan
 */

public abstract class BaseServiceException extends RuntimeException{

    /**
     * 状态码对应异常信息
     */
    protected HashMap<Integer,String> maps = new HashMap<>();
    /**
     * 异常状态码
     */
    @Getter
    protected int code;

    @Override
    public String getMessage() {
        return maps.get(code);
    }

    public BaseServiceException putIt(BaseCode code, String message) {
        maps.put(code.value(),message);
        return this;
    }

    public BaseServiceException(BaseCode code) {
        this.code = code.value();
    }

}
