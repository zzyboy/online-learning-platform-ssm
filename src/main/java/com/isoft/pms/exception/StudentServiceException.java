package com.isoft.pms.exception;

import com.isoft.pms.exception.baseException.BaseServiceException;
import com.isoft.pms.unit.StudentCode;

/**
 * 学生业务异常类
 * @author gaixiankang
 */
public class StudentServiceException extends BaseServiceException {

    public StudentServiceException(StudentCode code) {
        super(code);
        putIt(StudentCode.STUDENT_USER_ID_ISNULL,"用户编码为空").
                putIt(StudentCode.STUDENT_ID_ISNULL,"学生编码为空").
                putIt(StudentCode.STUDENT_NAME_ISNULL,"学生姓名为空").
                putIt(StudentCode.STUDENT_SEX_ISNULL,"学生性别为空").
                putIt(StudentCode.STUDENT_MAJOR_ID_ISNULL,"专业编码为空").
                putIt(StudentCode.STUDENT_STATUS_ISNULL,"状态码为空").
                putIt(StudentCode.STUDENT_USER_ID_NOT_EXIST,"用户编码不存在").
                putIt(StudentCode.STUDENT_NAME_SIZE_OUT50,"学生姓名不超过50字").
                putIt(StudentCode.STUDENT_MAJOR_ID_NOT_EXIST,"专业编码不存在").
                putIt(StudentCode.STUDENT_STATUS_NOT_EXIST,"学生状态码不存在").
                putIt(StudentCode.STUDENT_ID_ALREADY_EXIST,"学生编码已存在").
                putIt(StudentCode.STUDENT_SEX_NOT_EXIST,"学生性别不存在").
                putIt(StudentCode.STUDENT_USER_ID_ALREADY_EXIST,"该用户已被其他角色占用");

    }
}
